﻿namespace SecondMonitor.SimdataManagement
{
    using Contracts.TrackMap;
    using ViewModels.Settings;

    public class MapsLoaderFactory : IMapsLoaderFactory
    {
        private readonly ISettingsProvider _settingsProvider;

        public MapsLoaderFactory(ISettingsProvider settingsProvider)
        {
            _settingsProvider = settingsProvider;
        }

        public IMapsLoader Create()
        {
            return new MapsLoader(_settingsProvider.MapRepositoryPath);
        }
    }
}