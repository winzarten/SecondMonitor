﻿namespace SecondMonitor.F12024Connector.DataModels
{
    internal class AdditionalData
    {
        public bool[] RetiredDrivers = new bool[22];
        public SafetyCar LastSafetyCarUpdate = new SafetyCar();
    }
}