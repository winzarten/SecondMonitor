﻿namespace SecondMonitor.F12024Connector.DataModels.Enums
{
    internal enum DriverResultKind
    {
        Na,
        Inactive,
        Active,
        Finished,
        Disqualified,
        NotClassified,
        Retired,
    }
}