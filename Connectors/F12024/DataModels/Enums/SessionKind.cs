﻿namespace SecondMonitor.F12024Connector.DataModels.Enums
{
    public enum SessionKind
    {
        Na,
        Practice1,
        Practice2,
        Practice3,
        ShortPractice,
        Qualification1,
        Qualification2,
        Qualification3,
        ShortQualification,
        OnlineQualification,
        SprintShootout1,
        SprintShootout2,
        SprintShootout3,
        ShortSprintShootout,
        OneShotSprintShootout,
        Race1,
        Race2,
        Race3,
        TimeTrial,
    }
}