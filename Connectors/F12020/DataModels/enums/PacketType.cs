﻿namespace SecondMonitor.F12020Connector.DataModels.Enums
{
    public enum PacketType
    {
        Motion = 0,
        Session = 1,
        LapData = 2,
        Event = 3,
        Participants = 4,
        CarSetup = 5,
        CarTelemetry = 6,
        CarStatus = 7,
        FinalClassification = 8,
        LobbyInfo = 9
    }
}