﻿namespace SecondMonitor.LmUConnector.Content
{
    using System.Collections.Generic;
    using System.Linq;

    using SecondMonitor.DataModel.Extensions;

    public class LmUCar
    {
        public LmUCar(string carName, string teamName, int number)
        {
            CarName = carName;
            TeamName = teamName;
            Number = number;
        }

        public string CarName { get; }
        public string TeamName { get; }
        public int Number { get; }
    }

    public class LmUContent
    {
        private static readonly string[] suffixes = [":EC", ":AL", ":LM", ":PL"];
        private static readonly Dictionary<string, LmUCar> contentRaw = new Dictionary<string, LmUCar>();
        /*{
            { "Vista AF Corse 2024 #54", new LmUCar(Cars.Ferrari296GT3, Teams.VistaAFCorse, 54) },
            { "Vista AF Corse 2024 #55", new LmUCar(Cars.Ferrari296GT3, Teams.VistaAFCorse, 55) },
            { "United Autosports 2024 #59", new LmUCar(Cars.McLaren720sGT3, Teams.UnitedAutosport, 59) },
            { "United Autosports 2024 #95", new LmUCar(Cars.McLaren720sGT3, Teams.UnitedAutosport, 95) },
            { "TF Sport #81", new LmUCar(Cars.ChevroletCorvetteGT3, Teams.TfSport, 81) },
            { "TF Sport #82", new LmUCar(Cars.ChevroletCorvetteGT3, Teams.TfSport, 82) },
            { "Team WRT 2024 #46", new LmUCar(Cars.BMWM4Gt3, Teams.TeamWRT, 46) },
            { "Team WRT 2024 #31", new LmUCar(Cars.BMWM4Gt3, Teams.TeamWRT, 46) },
            { "JMW Motorsport 2024 2024 #66", new LmUCar(Cars.Ferrari296GT3, Teams.JMWMotosport, 66) },
            { "Inception Racing 2024 #70", new LmUCar(Cars.McLaren720sGT3, Teams.InceptionRacing, 70) },
            { "GR Racing 2024 #86", new LmUCar(Cars.Ferrari296GT3, Teams.GrRacing, 86) },
            { "Spirit Of Race 2024 #155", new LmUCar(Cars.Ferrari296GT3, Teams.SpiritOfRace, 155) },
        };*/

        private readonly Dictionary<string, LmUCar> _content;

        public LmUContent()
        {
            _content = contentRaw.ToDictionary();
            foreach (string suffix in suffixes)
            {
                contentRaw.ForEach(x => _content.Add(x.Key + suffix, x.Value));
            }
        }

        public LmUCar GetCarSafe(string lmuCarName)
        {
            return _content.GetValueOrDefault(lmuCarName, new LmUCar(lmuCarName, string.Empty, -1));
        }
    }
}
