﻿namespace SecondMonitor.LmUConnector.Content
{
    public static class Teams
    {
        // Hypercars
        public const string CadillacRacing = "Cadillac Racing";
        public const string Vanwall = "Floyd Vanwall Racing Team";
        public const string PorschePenske = "Porsche Penske Motorsport";
        public const string ToyotaRacing = "Toyota Gazoo Racing";
        public const string HertzTeamJota = "Hertz Team Jota";
        public const string FerrariAfCorse = "Ferrari AF Corse";
        public const string Peugeot = "Peugeot TotalEnergies";
        public const string ProtonCompetition = "Proton Competition";
        public const string GlickenHouse = "Glickenhaus Racing";
        public const string Isotta = "Isotta Fraschini";
        public const string BmwMTeam = "BMW M Team WRT";
        public const string AlpineEndurance = "Alpine Endurance Team";
        public const string LamborghiniIronLynx = "Lamborghini Iron Lynx";

        // LMP2
        public const string PremaRacing = "Prema Racing";
        public const string VectorSport = "Vector Sport";
        public const string UnitedAutosport = "United Autosports";
        public const string Jota = "Jota";
        public const string TeamWRT = "Team WRT";
        public const string InterEuropol = "Inter Europol Competition";
        public const string AlpineElf = "Alpine Elf Team";

        // GTE + GT3
        public const string AfCorse = "AF Corse";
        public const string RichardMille = "Richard Mille AF Corse";
        public const string ORT = "ORT by TF";
        public const string DstationRacing = "D'station Racing";
        public const string CorvetteRacing = "Corvette Racing";
        public const string Project1 = "Project 1 – AO";
        public const string KesselRacing = "Kessel Racing";
        public const string IronLynx = "Iron Lynx";
        public const string IronDames = "Iron Dames";
        public const string Dempsey = "Dempsey-Proton Racing[";
        public const string Proton = "Proton Competition";
        public const string GrRacing = "GR Racing";
        public const string NorthWest = "NorthWest AMR";

        public const string HeartRacingTeam = "Heart of Racing Team";
        public const string VistaAFCorse = "Vista AF Corse";
        public const string Akkodis = "Akkodis ASP Team";
        public const string TfSport = "TF Sport";
        public const string MantheyEMA = "Manthey EMA";
        public const string MantheyPureRxcing = "Manthey PureRxcing";
        public const string JMWMotosport = "JMW Motorsport";
        public const string InceptionRacing = "Inception Racing";
        public const string SpiritOfRace = "Spirit of Race";
    }
}
