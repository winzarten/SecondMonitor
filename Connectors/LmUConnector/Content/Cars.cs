﻿namespace SecondMonitor.LmUConnector.Content
{
    public static class Cars
    {
        // Hypercars
        public const string CadillacVSeries = "Cadillac V-Series.R";
        public const string VanwallVandervell = "Vanwall Vandervell 680";
        public const string Porsche963 = "Porsche 963";
        public const string ToyotaGR010 = "Toyota GR010 Hybrid";
        public const string Ferrari499P = "Ferrari 499P";
        public const string Peugeot9X8 = "Peugeot 9X8";
        public const string GlickenhausSCG007 = "Glickenhaus SCG 007 LMH";
        public const string Isotta = "Isotta Fraschini Tipo 6-C";
        public const string BmwHybridV8 = "BMW M Hybrid V8";
        public const string AlpineA424 = "Alpine A424";
        public const string LamborghiniSc63 = "Lamborghini SC63";

        // LMP2
        public const string Oreca07 = "Oreca 07";

        // GTE
        public const string Ferrari488GTE = "Ferrari 488 GTE Evo";
        public const string AstonMartinGTE = "Aston Martin Vantage AMR";
        public const string ChevroletCorvetteGTE = "Chevrolet Corvette C8.R";
        public const string Porsche911GTE = "Porsche 911 RSR-19";

        // LMGT3
        public const string AstonMartinGT3 = "Aston Martin Vantage AMR GT3 Evo";
        public const string BMWM4Gt3 = "BMW M4 GT3";
        public const string Ferrari296GT3 = "Ferrari 296 GT3";
        public const string McLaren720sGT3 = "McLaren 720S GT3 Evo";
        public const string LamborghiniHuracanGT3 = "Lamborghini Huracán GT3 Evo 2";
        public const string FordMustangGT3 = "Ford Mustang GT3";
        public const string LexusRcGT3 = "Lexus RC F GT3";
        public const string ChevroletCorvetteGT3 = "Chevrolet Corvette Z06 GT3.R";
        public const string Porsche911GT3 = "Porsche 911 GT3 R (992)";
    }
}
