﻿namespace SecondMonitor.LmUConnector.SharedMemory
{
    using System;
    using System.Linq;

    using SecondMonitor.DataModel;
    using SecondMonitor.DataModel.BasicProperties;
    using SecondMonitor.DataModel.Extensions;
    using SecondMonitor.DataModel.Snapshot;
    using SecondMonitor.DataModel.Snapshot.Drivers;
    using SecondMonitor.DataModel.Snapshot.Systems;
    using SecondMonitor.Foundation.Connectors;
    using SecondMonitor.Foundation.Connectors.Visitor;
    using SecondMonitor.LmUConnector.Content;
    using SecondMonitor.LmUConnector.SharedMemory.rFactor2Data;

    internal class LmUDataConverter : AbstractDataConvertor
    {
        private const int MaxConsecutivePackagesIgnored = 200;
        private readonly SessionTimeInterpolator _sessionTimeInterpolator;
        private readonly LmUContent _lmUContent;

        private DriverInfo _lastPlayer = new();
        private int _lastPlayerId = -1;
        private lmUVehicleTelemetry _lastPlayerTelemetry;

        private int _currentlyIgnoredPackage;
        //private TimeSpan _lastPlayerTime = TimeSpan.Zero;

        public LmUDataConverter(SessionTimeInterpolator sessionTimeInterpolator)
        {
            _sessionTimeInterpolator = sessionTimeInterpolator;
            _lmUContent = new LmUContent();
        }

        public SimulatorDataSet CreateSimulatorDataSet(in LmUFullData lmData, LmURestData restData)
        {
            try
            {
                SimulatorDataSet simData = new(SimulatorsNameMap.LmUSimName)
                {
                    SimulatorSourceInfo =
                    {
                        GapInformationProvided = GapInformationKind.None,
                        HasLapTimeInformation = true,
                        SimNotReportingEndOfOutLapCorrectly = true,
                        InvalidateLapBySector = true,
                        SectorTimingSupport = DataInputSupport.Full,
                        TelemetryInfo = { ContainsSuspensionTravel = true, ContainsWheelRps = true, ContainsTyreLoad = true },
                    }
                };
                //simData.SimulatorSourceInfo.TelemetryInfo.ContainsSuspensionVelocity = true;
                /*                simData.SimulatorSourceInfo.TelemetryInfo.RequiresDistanceInterpolation = true;
                                simData.SimulatorSourceInfo.TelemetryInfo.RequiresPositionInterpolation = true;*/

                FillSessionInfo(lmData, simData);
                AddFlags(lmData, simData);
                AddDriversData(simData, lmData, restData);

                if (_lastPlayerId == -1)
                {
                    return simData;
                }

                lmUVehicleTelemetry playelmUVehicleTelemetry = lmData.telemetry.mVehicles.FirstOrDefault(x => x.mID == _lastPlayerId && x.mElapsedTime > 0);

                //This is unreliable, it resets at long races
                /*if (playelmUVehicleTelemetry.mElapsedTime > 0)
                {
                    simData.SessionInfo.SessionTime = TimeSpan.FromSeconds(playelmUVehicleTelemetry.mElapsedTime);
                }*/

                if (playelmUVehicleTelemetry.mWheels == null && _lastPlayerTelemetry.mWheels != null)
                {
                    playelmUVehicleTelemetry = _lastPlayerTelemetry;
                }
                else
                {
                    _lastPlayerTelemetry = playelmUVehicleTelemetry;
                }

                if (playelmUVehicleTelemetry.mWheels == null)
                {
                    return simData;
                }

                FillPlayerCarInfo(playelmUVehicleTelemetry, simData);

                // PEDAL INFO
                AddPedalInfo(playelmUVehicleTelemetry, simData);

                // WaterSystemInfo
                AddWaterSystemInfo(playelmUVehicleTelemetry, simData);

                // OilSystemInfo
                AddOilSystemInfo(playelmUVehicleTelemetry, simData);

                // Brakes Info
                AddBrakesInfo(playelmUVehicleTelemetry, simData);

                // Tyre Pressure Info
                AddTyresAndFuelInfo(simData, playelmUVehicleTelemetry);

                // Acceleration
                AddAcceleration(simData, playelmUVehicleTelemetry);

                //Add Additional Player Car Info
                AddPlayerCarInfo(playelmUVehicleTelemetry, simData);

                _currentlyIgnoredPackage = 0;

                PopulateClassPositions(simData);

                FillSpectateInfo(simData, lmData);

                FillRestData(simData, restData);

                return simData;
            }
            catch (Exception ex)
            {
                _lastPlayerId = -1;
                _lastPlayer = new DriverInfo();
                throw new LMUInvalidPackageException(ex);
            }
        }

        private void FillRestData(SimulatorDataSet simData, LmURestData restData)
        {
            if (restData.CurrentVirtualEnergy <= 1 || restData.MaxVirtualEnergy <= 0 || simData.PlayerInfo.CarInfo == null)
            {
                return;
            }

            simData.PlayerInfo.CarInfo.FuelSystemInfo.HasVirtualTank = true;
            simData.PlayerInfo.CarInfo.FuelSystemInfo.VirtualTank = Percentage.FromRatio(restData.CurrentVirtualEnergy / restData.MaxVirtualEnergy);
        }

        private void FillSpectateInfo(SimulatorDataSet data, in LmUFullData lmData)
        {
            if (_lastPlayerId == -1)
            {
                data.SessionInfo.SpectatingState = SpectatingState.Spectate;
            }

            data.SessionInfo.SpectatingState = lmData.scoring.mVehicles[_lastPlayerId].mControl != 3 ? SpectatingState.Live : SpectatingState.Spectate;
        }

        internal static DriverFinishStatus FromRfStatus(int finishStatus)
        {
            return (rFactor2Constants.lmUFinishStatus)finishStatus switch
            {
                rFactor2Constants.lmUFinishStatus.None => DriverFinishStatus.None,
                rFactor2Constants.lmUFinishStatus.Dnf => DriverFinishStatus.Dnf,
                rFactor2Constants.lmUFinishStatus.Dq => DriverFinishStatus.Dq,
                rFactor2Constants.lmUFinishStatus.Finished => DriverFinishStatus.Finished,
                _ => DriverFinishStatus.Na
            };
        }

        private static void AddBrakesInfo(lmUVehicleTelemetry playerVehicleTelemetry, SimulatorDataSet simData)
        {
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontLeft.BrakeTemperature.ActualQuantity = Temperature.FromKelvin(
                playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.FrontLeft].mBrakeTemp);
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontRight.BrakeTemperature.ActualQuantity = Temperature.FromKelvin(
                playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.FrontRight].mBrakeTemp);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearLeft.BrakeTemperature.ActualQuantity = Temperature.FromKelvin(
                playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.RearLeft].mBrakeTemp);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearRight.BrakeTemperature.ActualQuantity = Temperature.FromKelvin(
                playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.RearRight].mBrakeTemp);
        }

        private static void AddOilSystemInfo(lmUVehicleTelemetry playerVehicleTelemetry, SimulatorDataSet simData)
        {
            simData.PlayerInfo.CarInfo.OilSystemInfo.OptimalOilTemperature.ActualQuantity = Temperature.FromCelsius(playerVehicleTelemetry.mEngineOilTemp);
            simData.PlayerInfo.CarInfo.TurboPressure = Pressure.FromKiloPascals(playerVehicleTelemetry.mTurboBoostPressure / 1000);
        }

        private static void AddWaterSystemInfo(lmUVehicleTelemetry playerVehicleTelemetry, SimulatorDataSet simData)
        {
            simData.PlayerInfo.CarInfo.WaterSystemInfo.OptimalWaterTemperature.ActualQuantity = Temperature.FromCelsius(playerVehicleTelemetry.mEngineWaterTemp);
        }

        private static void AddPedalInfo(lmUVehicleTelemetry playerVehicleTelemetry, SimulatorDataSet simData)
        {
            simData.InputInfo.ThrottlePedalPosition = playerVehicleTelemetry.mUnfilteredThrottle;
            simData.InputInfo.BrakePedalPosition = playerVehicleTelemetry.mUnfilteredBrake;
            simData.InputInfo.ClutchPedalPosition = playerVehicleTelemetry.mUnfilteredClutch;
            simData.InputInfo.SteeringInput = playerVehicleTelemetry.mUnfilteredSteering;
        }

        private void AddPlayerCarInfo(lmUVehicleTelemetry data, SimulatorDataSet simData)
        {
            CarInfo playerCar = simData.PlayerInfo.CarInfo;

            int totalDent = data.mDentSeverity.Aggregate((x, y) => (byte)(x + y));
            int maxDent = data.mDentSeverity.Max();
            playerCar.CarDamageInformation.Bodywork.Damage = totalDent / 16.0;
            if (maxDent == 1)
            {
                playerCar.CarDamageInformation.Bodywork.MediumDamageThreshold = playerCar.CarDamageInformation.Bodywork.Damage;
            }
            else if (maxDent == 2)
            {
                playerCar.CarDamageInformation.Bodywork.MediumDamageThreshold = 0;
                playerCar.CarDamageInformation.Bodywork.HeavyDamageThreshold = playerCar.CarDamageInformation.Bodywork.Damage;
            }

            if (data.mOverheating == 1)
            {
                playerCar.CarDamageInformation.Engine.Damage = 1;
            }

            /*playerCar.WorldOrientation = new Orientation()
            {
                Yaw = Angle.GetFromRadians(Math.Atan2(data.mOri[2].x, data.mOri[2].z)),
            };*/

            playerCar.SpeedLimiterEngaged = data.mSpeedLimiter == 1;
            playerCar.FrontDownForce = Force.GetFromNewtons(data.mFrontDownforce);
            playerCar.RearDownForce = Force.GetFromNewtons(data.mRearDownforce);
            playerCar.OverallDownForce = Force.GetFromNewtons(data.mFrontDownforce + data.mRearDownforce);
        }

        private void AddFlags(in LmUFullData lmData, SimulatorDataSet simData)
        {
            if ((rFactor2Constants.lmUGamePhase)lmData.scoring.mScoringInfo.mGamePhase == rFactor2Constants.lmUGamePhase.FullCourseYellow)
            {
                simData.SessionInfo.ActiveFlags |= FlagKind.FullCourseYellow;
                return;
            }

            if (lmData.scoring.mScoringInfo.mSectorFlag[0] == 1)
            {
                simData.SessionInfo.ActiveFlags |= FlagKind.YellowSector1;
            }

            if (lmData.scoring.mScoringInfo.mSectorFlag[1] == 1)
            {
                simData.SessionInfo.ActiveFlags |= FlagKind.YellowSector2;
            }

            if (lmData.scoring.mScoringInfo.mSectorFlag[2] == 1)
            {
                simData.SessionInfo.ActiveFlags |= FlagKind.YellowSector3;
            }
        }

        private void AddAcceleration(SimulatorDataSet simData, lmUVehicleTelemetry playerVehicleTelemetry)
        {
            simData.PlayerInfo.CarInfo.Acceleration.XinMs = playerVehicleTelemetry.mLocalAccel.x;
            simData.PlayerInfo.CarInfo.Acceleration.YinMs = playerVehicleTelemetry.mLocalAccel.y;
            simData.PlayerInfo.CarInfo.Acceleration.ZinMs = -playerVehicleTelemetry.mLocalAccel.z;
        }

        private void AddTyresAndFuelInfo(SimulatorDataSet simData, lmUVehicleTelemetry playerVehicleTelemetry)
        {
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontLeft.TyrePressure.ActualQuantity =
                Pressure.FromKiloPascals(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.FrontLeft].mPressure);
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontRight.TyrePressure.ActualQuantity =
                Pressure.FromKiloPascals(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.FrontRight].mPressure);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearLeft.TyrePressure.ActualQuantity =
                Pressure.FromKiloPascals(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.RearLeft].mPressure);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearRight.TyrePressure.ActualQuantity =
                Pressure.FromKiloPascals(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.RearRight].mPressure);

            simData.PlayerInfo.CarInfo.WheelsInfo.FrontLeft.Detached = playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.FrontLeft].mDetached == 1 || playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.FrontLeft].mFlat == 1;
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontRight.Detached = playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.FrontRight].mDetached == 1 || playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.FrontRight].mFlat == 1;
            simData.PlayerInfo.CarInfo.WheelsInfo.RearLeft.Detached = playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.RearLeft].mDetached == 1 || playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.RearLeft].mFlat == 1;
            simData.PlayerInfo.CarInfo.WheelsInfo.RearRight.Detached = playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.RearRight].mDetached == 1 || playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.RearRight].mFlat == 1;

            simData.PlayerInfo.CarInfo.RearHeight = Distance.FromMeters(playerVehicleTelemetry.mRearRideHeight);
            simData.PlayerInfo.CarInfo.FrontHeight = Distance.FromMeters(playerVehicleTelemetry.mFrontWingHeight);

            simData.PlayerInfo.CarInfo.WheelsInfo.FrontLeft.Camber = Angle.GetFromRadians(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.FrontLeft].mCamber);
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontRight.Camber = Angle.GetFromRadians(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.FrontRight].mCamber);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearLeft.Camber = Angle.GetFromRadians(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.RearLeft].mCamber);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearRight.Camber = Angle.GetFromRadians(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.RearRight].mCamber);

            //Tyre RPS
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontLeft.Rps = -playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.FrontLeft].mRotation;
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontRight.Rps = -playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.FrontRight].mRotation;
            simData.PlayerInfo.CarInfo.WheelsInfo.RearLeft.Rps = -playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.RearLeft].mRotation;
            simData.PlayerInfo.CarInfo.WheelsInfo.RearRight.Rps = -playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.RearRight].mRotation;

            //Ride Tyre Height
            simData.PlayerInfo.CarInfo.WheelsInfo.IsRideHeightFilled = true;
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontLeft.RideHeight = Distance.FromMeters(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.FrontLeft].mRideHeight);
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontRight.RideHeight = Distance.FromMeters(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.FrontRight].mRideHeight);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearLeft.RideHeight = Distance.FromMeters(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.RearLeft].mRideHeight);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearRight.RideHeight = Distance.FromMeters(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.RearRight].mRideHeight);

            //Tyre Load
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontLeft.TyreLoad = Force.GetFromNewtons(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.FrontLeft].mTireLoad);
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontRight.TyreLoad = Force.GetFromNewtons(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.FrontRight].mTireLoad);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearLeft.TyreLoad = Force.GetFromNewtons(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.RearLeft].mTireLoad);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearRight.TyreLoad = Force.GetFromNewtons(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.RearRight].mTireLoad);

            //Suspension Deflection
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontLeft.SuspensionTravel = Distance.FromMeters(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.FrontLeft].mSuspensionDeflection);
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontRight.SuspensionTravel = Distance.FromMeters(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.FrontRight].mSuspensionDeflection);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearLeft.SuspensionTravel = Distance.FromMeters(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.RearLeft].mSuspensionDeflection);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearRight.SuspensionTravel = Distance.FromMeters(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.RearRight].mSuspensionDeflection);

            simData.PlayerInfo.CarInfo.WheelsInfo.FrontLeft.TyreWear.ActualWear =
                1 - playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.FrontLeft].mWear;
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontRight.TyreWear.ActualWear =
                1 - playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.FrontRight].mWear;
            simData.PlayerInfo.CarInfo.WheelsInfo.RearLeft.TyreWear.ActualWear =
                1 - playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.RearLeft].mWear;
            simData.PlayerInfo.CarInfo.WheelsInfo.RearRight.TyreWear.ActualWear =
                1 - playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.RearRight].mWear;

            // Front Left Tyre Temps
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontLeft.LeftTyreTemp.ActualQuantity =
                Temperature.FromKelvin(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.FrontLeft].mTemperature[0]);
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontLeft.RightTyreTemp.ActualQuantity =
                Temperature.FromKelvin(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.FrontLeft].mTemperature[2]);
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontLeft.CenterTyreTemp.ActualQuantity =
                Temperature.FromKelvin(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.FrontLeft].mTemperature[1]);
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontLeft.TyreCoreTemperature.ActualQuantity =
                Temperature.FromKelvin(Math.Min(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.FrontLeft].mTireCarcassTemperature, 2000));

            // Front Right Tyre Temps
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontRight.LeftTyreTemp.ActualQuantity =
                Temperature.FromKelvin(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.FrontRight].mTemperature[0]);
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontRight.RightTyreTemp.ActualQuantity =
                Temperature.FromKelvin(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.FrontRight].mTemperature[2]);
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontRight.CenterTyreTemp.ActualQuantity =
                Temperature.FromKelvin(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.FrontRight].mTemperature[1]);
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontRight.TyreCoreTemperature.ActualQuantity =
                Temperature.FromKelvin(Math.Min(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.FrontRight].mTireCarcassTemperature, 2000));

            // Rear Left Tyre Temps
            simData.PlayerInfo.CarInfo.WheelsInfo.RearLeft.LeftTyreTemp.ActualQuantity =
                Temperature.FromKelvin(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.RearLeft].mTemperature[0]);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearLeft.RightTyreTemp.ActualQuantity =
                Temperature.FromKelvin(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.RearLeft].mTemperature[2]);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearLeft.CenterTyreTemp.ActualQuantity =
                Temperature.FromKelvin(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.RearLeft].mTemperature[1]);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearLeft.TyreCoreTemperature.ActualQuantity =
                Temperature.FromKelvin(Math.Min(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.RearLeft].mTireCarcassTemperature, 2000));

            // Rear Right Tyre Temps
            simData.PlayerInfo.CarInfo.WheelsInfo.RearRight.LeftTyreTemp.ActualQuantity =
                Temperature.FromKelvin(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.RearRight].mTemperature[0]);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearRight.RightTyreTemp.ActualQuantity =
                Temperature.FromKelvin(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.RearRight].mTemperature[2]);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearRight.CenterTyreTemp.ActualQuantity =
                Temperature.FromKelvin(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.RearRight].mTemperature[1]);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearRight.TyreCoreTemperature.ActualQuantity =
                Temperature.FromKelvin(Math.Min(playerVehicleTelemetry.mWheels[(int)rFactor2Constants.lmUWheelIndex.RearRight].mTireCarcassTemperature, 2000));

            // Fuel System
            simData.PlayerInfo.CarInfo.FuelSystemInfo.FuelCapacity = Volume.FromLiters(playerVehicleTelemetry.mFuelCapacity);
            simData.PlayerInfo.CarInfo.FuelSystemInfo.FuelRemaining = Volume.FromLiters(playerVehicleTelemetry.mFuel);
        }

        private void FillPlayerCarInfo(lmUVehicleTelemetry playerVehicleTelemetry, SimulatorDataSet simData)
        {
            //TimeSpan playerSessionTime = TimeSpan.FromSeconds(playerVehicleTelemetry.mElapsedTime);
            //simData.SessionInfo.SessionTime = playerSessionTime;
            /*if (playerSessionTime > simData.SessionInfo.SessionTime && playerSessionTime > _lastPlayerTime)
            {
                simData.SessionInfo.SessionTime = playerSessionTime;
                _lastPlayerTime = playerSessionTime;
            }
            else
            {
                playerSessionTime = TimeSpan.Zero;
            }*/
            FillBoostInfo(playerVehicleTelemetry, simData);
            simData.PlayerInfo.CarInfo.EngineRpm = (int)playerVehicleTelemetry.mEngineRPM;
            simData.PlayerInfo.CarInfo.CurrentGear = playerVehicleTelemetry.mGear switch
            {
                0 => "N",
                -1 => "R",
                -2 => string.Empty,
                _ => playerVehicleTelemetry.mGear.ToString()
            };
        }

        private void FillBoostInfo(lmUVehicleTelemetry playerVehicleTelemetry, SimulatorDataSet simData)
        {
            if (playerVehicleTelemetry.mElectricBoostMotorState == 0)
            {
                return;
            }

            HybridSystem hybridSystem = simData.PlayerInfo.CarInfo.HybridSystem;
            hybridSystem.RemainingChargePercentage = playerVehicleTelemetry.mBatteryChargeFraction;
            hybridSystem.HybridSystemMode = HybridSystemMode.Balanced;

            switch (playerVehicleTelemetry.mElectricBoostMotorState)
            {
                case 1:
                    hybridSystem.HybridChargeChange = HybridChargeChange.None;
                    return;
                case 2:
                    hybridSystem.HybridChargeChange = HybridChargeChange.Discharge;
                    return;
                case 3:
                    hybridSystem.HybridChargeChange = HybridChargeChange.Recharge;
                    return;
                default:
                    return;
            }
        }

        internal void AddDriversData(SimulatorDataSet data, in LmUFullData lmData, LmURestData restData)
        {
            if (lmData.scoring.mScoringInfo.mNumVehicles < 1)
            {
                return;
            }

            data.DriversInfo = new DriverInfo[lmData.scoring.mScoringInfo.mNumVehicles];
            DriverInfo playersInfo = null;

            for (int i = 0; i < lmData.scoring.mScoringInfo.mNumVehicles; i++)
            {
                lmUVehicleScoring lmUVehicleScoring = lmData.scoring.mVehicles[i];
                lmUVehicleTelemetry lmUVehicleTelemetry = lmData.telemetry.mVehicles[i];
                DriverInfo driverInfo = CreateDriverInfo(lmData, lmUVehicleScoring);

                if (driverInfo.IsPlayer)
                {
                    driverInfo.CarInfo.HeadLightsStatus = lmUVehicleScoring.mHeadlights == 1 ? HeadLightsStatus.NormalBeams : HeadLightsStatus.Off;
                    playersInfo = driverInfo;
                    driverInfo.CurrentLapValid = true;
                    _lastPlayerId = lmUVehicleScoring.mID;
                }
                else
                {
                    driverInfo.CurrentLapValid = true;
                }

                FillTyreCompounds(driverInfo, lmUVehicleTelemetry, lmUVehicleScoring, restData);

                data.DriversInfo[i] = driverInfo;
                if (driverInfo.Position == 1)
                {
                    data.SessionInfo.LeaderCurrentLap = driverInfo.CompletedLaps + 1;
                    data.LeaderInfo = driverInfo;
                }

                if (lmUVehicleScoring.mControl == 2)
                {
                    data.SessionInfo.IsMultiplayer = true;
                }

                AddLappingInformation(data, lmData, driverInfo);
                FillTimingInfo(driverInfo, lmUVehicleScoring, lmData);

                if (lmData.scoring.mScoringInfo.mSectorFlag[0] == 1 || lmData.scoring.mScoringInfo.mSectorFlag[1] == 1 || lmData.scoring.mScoringInfo.mSectorFlag[2] == 1)
                {
                    driverInfo.IsCausingYellow = !driverInfo.InPits && driverInfo.FinishStatus == DriverFinishStatus.None && driverInfo.Speed.InKph < 40;
                }

                if (driverInfo.FinishStatus == DriverFinishStatus.Finished && !driverInfo.IsPlayer && driverInfo.Position > _lastPlayer.Position)
                {
                    driverInfo.CompletedLaps--;
                    driverInfo.FinishStatus = DriverFinishStatus.None;
                }
            }

            CheckValidityByPlayer(playersInfo);
            _lastPlayer = playersInfo;
            if (playersInfo != null)
            {
                data.PlayerInfo = playersInfo;
            }

            FillGapInformation(data.DriversInfo);
        }

        private void FillGapInformation(DriverInfo[] drivers)
        {
            DriverInfo[] orderedDrivers = drivers.OrderBy(x => x.Position).ToArray();

            for (int i = 1; i < orderedDrivers.Length; i++)
            {
                orderedDrivers[i - 1].Timing.GapBehind = orderedDrivers[i].Timing.GapAhead;
            }
        }

        private void CheckValidityByPlayer(DriverInfo driver)
        {
            if (_lastPlayer == null || driver == null || (!_lastPlayer.InPits && driver.InPits))
            {
                return;
            }

            Distance distance = Point3D.GetDistance(driver.WorldPosition, _lastPlayer.WorldPosition);
            if (distance.InMeters > 200)
            {
                _currentlyIgnoredPackage++;
                if (_currentlyIgnoredPackage < MaxConsecutivePackagesIgnored)
                {
                    throw new LMUInvalidPackageException("Players distance was :" + distance.InMeters);
                }
            }
        }

        private void FillTyreCompounds(DriverInfo driverInfo, lmUVehicleTelemetry lmUVehicleTelemetry, lmUVehicleScoring vehicleScoring, LmURestData lmURestData)
        {
            if (lmURestData.DriversData.TryGetValue(vehicleScoring.mPlace, out LmURestDriverData restDriverData))
            {
                driverInfo.CarInfo.WheelsInfo.FrontLeft.TyreType = restDriverData.FrontLeftCompound;
                driverInfo.CarInfo.WheelsInfo.FrontRight.TyreType = restDriverData.FrontRightCompound;
                driverInfo.CarInfo.WheelsInfo.RearRight.TyreType = restDriverData.RearRightCompound;
                driverInfo.CarInfo.WheelsInfo.RearLeft.TyreType = restDriverData.RearLeftCompound;
            }
            else
            {
                driverInfo.CarInfo.WheelsInfo.FrontLeft.TyreType = lmUVehicleTelemetry.mFrontTireCompoundName.FromArray();
                driverInfo.CarInfo.WheelsInfo.FrontRight.TyreType = driverInfo.CarInfo.WheelsInfo.FrontLeft.TyreType;
                driverInfo.CarInfo.WheelsInfo.RearRight.TyreType = lmUVehicleTelemetry.mRearTireCompoundName.FromArray();
                driverInfo.CarInfo.WheelsInfo.RearLeft.TyreType = driverInfo.CarInfo.WheelsInfo.RearRight.TyreType;
            }
        }

        internal void FillTimingInfo(DriverInfo driverInfo, lmUVehicleScoring rfVehicleInfo, in LmUFullData lmUFullData)
        {
            driverInfo.Timing.GapAhead = TimeSpan.FromSeconds(rfVehicleInfo.mTimeBehindNext);
            driverInfo.Timing.LastSector1Time = CreateTimeSpan(rfVehicleInfo.mCurSector1);
            driverInfo.Timing.LastSector2Time = CreateTimeSpan(rfVehicleInfo.mCurSector2 - rfVehicleInfo.mCurSector1);
            driverInfo.Timing.LastSector3Time = CreateTimeSpan(rfVehicleInfo.mLastLapTime - rfVehicleInfo.mLastSector2);
            driverInfo.Timing.LastLapTime = CreateTimeSpan(rfVehicleInfo.mLastLapTime);
            driverInfo.Timing.CurrentSector = rfVehicleInfo.mSector == 0 ? 3 : rfVehicleInfo.mSector;
            driverInfo.Timing.CurrentLapTime = CreateTimeSpan(lmUFullData.scoring.mScoringInfo.mCurrentET - rfVehicleInfo.mLapStartET);
        }

        private TimeSpan CreateTimeSpan(double seconds)
        {
            return seconds > 0 ? _sessionTimeInterpolator.ApplyInterpolation(TimeSpan.FromSeconds(seconds)) : TimeSpan.Zero;
        }

        private void AddLappingInformation(SimulatorDataSet data, in LmUFullData lmData, DriverInfo driverInfo)
        {
            if (data.SessionInfo.SessionType == SessionType.Race && _lastPlayer != null
                                                                 && _lastPlayer.CompletedLaps != 0)
            {
                driverInfo.IsBeingLappedByPlayer =
                    driverInfo.TotalDistance < (_lastPlayer.TotalDistance - (lmData.scoring.mScoringInfo.mLapDist * 0.5));
                driverInfo.IsLappingPlayer =
                    _lastPlayer.TotalDistance < (driverInfo.TotalDistance - (lmData.scoring.mScoringInfo.mLapDist * 0.5));
            }
        }

        private DriverInfo CreateDriverInfo(in LmUFullData lmData, lmUVehicleScoring rfVehicleInfo)
        {
            string lmuCarName = rfVehicleInfo.mVehicleName.FromArray();
            LmUCar lmUCar = _lmUContent.GetCarSafe(lmuCarName);
            DriverInfo driverInfo = new()
            {
                DriverSessionId = rfVehicleInfo.mDriverName.FromArray(),
                CompletedLaps = rfVehicleInfo.mTotalLaps,
                CarName = lmUCar.CarName,
                TeamName = lmUCar.TeamName,
                CarRaceNumber = lmUCar.Number,
                CarClassName = rfVehicleInfo.mVehicleClass.FromArray(),
                InPits = rfVehicleInfo.mInPits == 1,
                PitStopRequested = rfVehicleInfo.mPitState == 1,
            };
            driverInfo.DriverShortName = driverInfo.DriverLongName = driverInfo.DriverSessionId;
            driverInfo.CarClassId = driverInfo.CarClassName;
            driverInfo.IsPlayer = rfVehicleInfo.mIsPlayer == 1;
            driverInfo.Position = rfVehicleInfo.mPlace;
            driverInfo.Speed = Velocity.FromMs(Math.Sqrt((rfVehicleInfo.mLocalVel.x * rfVehicleInfo.mLocalVel.x)
                                                         + (rfVehicleInfo.mLocalVel.y * rfVehicleInfo.mLocalVel.y)
                                                         + (rfVehicleInfo.mLocalVel.z * rfVehicleInfo.mLocalVel.z)));
            driverInfo.LapDistance = rfVehicleInfo.mLapDist;
            driverInfo.TotalDistance = (rfVehicleInfo.mTotalLaps * lmData.scoring.mScoringInfo.mLapDist) + rfVehicleInfo.mLapDist;
            driverInfo.FinishStatus = FromRfStatus(rfVehicleInfo.mFinishStatus);
            driverInfo.WorldPosition = new Point3D(Distance.FromMeters(-rfVehicleInfo.mPos.x), Distance.FromMeters(rfVehicleInfo.mPos.y), Distance.FromMeters(rfVehicleInfo.mPos.z));
            ComputeDistanceToPlayer(_lastPlayer, driverInfo, lmData);
            return driverInfo;
        }

        internal static void ComputeDistanceToPlayer(DriverInfo player, DriverInfo driverInfo, in LmUFullData lmUFullData)
        {
            if (player == null)
            {
                return;
            }

            if (driverInfo.FinishStatus == DriverFinishStatus.Dq || driverInfo.FinishStatus == DriverFinishStatus.Dnf ||
                driverInfo.FinishStatus == DriverFinishStatus.Dnq || driverInfo.FinishStatus == DriverFinishStatus.Dns)
            {
                driverInfo.DistanceToPlayer = double.MaxValue;
                return;
            }

            double trackLength = lmUFullData.scoring.mScoringInfo.mLapDist;
            double playerLapDistance = player.LapDistance;

            double distanceToPlayer = playerLapDistance - driverInfo.LapDistance;
            if (distanceToPlayer < -(trackLength / 2))
            {
                distanceToPlayer = distanceToPlayer + trackLength;
            }

            if (distanceToPlayer > (trackLength / 2))
            {
                distanceToPlayer = distanceToPlayer - trackLength;
            }

            driverInfo.DistanceToPlayer = distanceToPlayer;
        }

        internal void FillSessionInfo(in LmUFullData data, SimulatorDataSet simData)
        {
            // Timing
            simData.SessionInfo.TrackInfo.LayoutLength = Distance.FromMeters(data.scoring.mScoringInfo.mLapDist);
            simData.SessionInfo.TrackInfo.TrackName = data.scoring.mScoringInfo.mTrackName.FromArray();
            simData.SessionInfo.TrackInfo.TrackLayoutName = string.Empty;
            simData.SessionInfo.WeatherInfo.AirTemperature = Temperature.FromCelsius(data.scoring.mScoringInfo.mAmbientTemp);
            simData.SessionInfo.WeatherInfo.TrackTemperature = Temperature.FromCelsius(data.scoring.mScoringInfo.mTrackTemp);
            simData.SessionInfo.WeatherInfo.RainIntensity = (int)(data.scoring.mScoringInfo.mRaining * 100);
            simData.SessionInfo.WeatherInfo.TrackWetness = (int)(((data.scoring.mScoringInfo.mMinPathWetness + data.scoring.mScoringInfo.mMaxPathWetness) / 2.0) * 100);
            simData.SessionInfo.SessionTime = TimeSpan.FromSeconds(data.scoring.mScoringInfo.mCurrentET);

            //_sessionTimeInterpolator.Visit(simData);

            if (data.extended.mTicksSessionEnded > data.extended.mTicksSessionStarted)
            {
                simData.SessionInfo.SessionType = SessionType.Na;
                simData.SessionInfo.SessionPhase = SessionPhase.Countdown;
                return;
            }

            if (data.scoring.mScoringInfo is { mTrackTemp: 0, mSession: 0, mGamePhase: 0 }
                && string.IsNullOrEmpty(simData.SessionInfo.TrackInfo.TrackName)
                && data.scoring.mScoringInfo.mLapDist == 0)
            {
                simData.SessionInfo.SessionType = SessionType.Na;
                simData.SessionInfo.SessionPhase = SessionPhase.Countdown;
                return;
            }

            simData.SessionInfo.SessionType = data.scoring.mScoringInfo.mSession switch
            {
                -1 => SessionType.Na,
                0 or 1 or 2 or 3 or 4 => SessionType.Practice,
                5 or 6 or 7 or 8 => SessionType.Qualification,
                9 => SessionType.WarmUp,
                10 or 11 or 12 or 13 => SessionType.Race,
                _ => SessionType.Practice
            };

            switch ((rFactor2Constants.lmUGamePhase)data.scoring.mScoringInfo.mGamePhase)
            {
                case rFactor2Constants.lmUGamePhase.Garage:
                    break;
                case rFactor2Constants.lmUGamePhase.WarmUp:
                case rFactor2Constants.lmUGamePhase.GridWalk:
                case rFactor2Constants.lmUGamePhase.Formation:
                case rFactor2Constants.lmUGamePhase.Countdown:
                    simData.SessionInfo.SessionPhase = SessionPhase.Countdown;
                    break;
                case rFactor2Constants.lmUGamePhase.SessionStopped:
                case rFactor2Constants.lmUGamePhase.FullCourseYellow:
                case rFactor2Constants.lmUGamePhase.GreenFlag:
                    simData.SessionInfo.SessionPhase = SessionPhase.Green;
                    break;
                case rFactor2Constants.lmUGamePhase.SessionOver:
                    simData.SessionInfo.SessionPhase = SessionPhase.Checkered;
                    break;
            }

            simData.SessionInfo.IsActive = simData.SessionInfo.SessionType != SessionType.Na;

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            if (data.scoring.mScoringInfo.mEndET > 0)
            {
                simData.SessionInfo.SessionLengthType = SessionLengthType.Time;
                simData.SessionInfo.SessionTimeRemaining =
                    data.scoring.mScoringInfo.mEndET - data.scoring.mScoringInfo.mCurrentET > 0 ? data.scoring.mScoringInfo.mEndET - data.scoring.mScoringInfo.mCurrentET : 0;
            }
            else
            {
                simData.SessionInfo.SessionLengthType = SessionLengthType.Laps;
                simData.SessionInfo.TotalNumberOfLaps = data.scoring.mScoringInfo.mMaxLaps;
            }
        }
    }
}