﻿namespace SecondMonitor.LmUConnector.SharedMemory
{
    public class LmURestDriverData
    {
        public LmURestDriverData(int position, string frontLeftCompound, string frontRightCompound, string rearLeftCompound, string rearRightCompound)
        {
            Position = position;
            FrontLeftCompound = frontLeftCompound;
            FrontRightCompound = frontRightCompound;
            RearLeftCompound = rearLeftCompound;
            RearRightCompound = rearRightCompound;
        }

        public int Position { get; set; }
        public string FrontLeftCompound { get; set; }
        public string FrontRightCompound { get; set; }
        public string RearLeftCompound { get; set; }
        public string RearRightCompound { get; set; }
    }
}
