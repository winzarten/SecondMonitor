﻿namespace SecondMonitor.LmUConnector.SharedMemory
{
    using System.Collections.Generic;

    public class LmURestData
    {
        public LmURestData()
        {
            DriversData = new Dictionary<int, LmURestDriverData>();
        }

        public LmURestData(double currentVirtualEnergy, double maxVirtualEnergy) : this()
        {
            CurrentVirtualEnergy = currentVirtualEnergy;
            MaxVirtualEnergy = maxVirtualEnergy;
        }

        public double CurrentVirtualEnergy { get; set; } = -1;

        public double MaxVirtualEnergy { get; set; } = -1;
        
        public Dictionary<int, LmURestDriverData> DriversData { get; }
    }
}
