﻿namespace SecondMonitor.LmUConnector
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Threading;
    using System.Threading.Tasks;

    using Newtonsoft.Json.Linq;

    using SecondMonitor.LmUConnector.SharedMemory;

    public class LmuRestConnector
    {
        private const string Url = @"http://localhost:6397";
        private const string NavigationUri = @"/navigation/state";
        private const string GarageInfoUri = @"/rest/garage/UIScreen/RepairAndRefuel";
        private const string StandingsUri = @"/rest/garage/UIScreen/Standings";
        private static readonly string[] invalidNavigationStates = new string[] { "NAV_MAIN_MENU" };

        private bool _isRunning;
        private CancellationTokenSource _cancellationTokenSource;
        private Task _loopTask;
        private HttpClient _client;

        public void Start()
        {
            if (_isRunning)
            {
                return;
            }

            _client = new HttpClient();
            _client.Timeout = TimeSpan.FromSeconds(2);
            _client.BaseAddress = new Uri(Url);
            _client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            _isRunning = true;
            _cancellationTokenSource = new CancellationTokenSource();
            _loopTask = LoopMethod(_cancellationTokenSource.Token);
        }

        public LmURestData LmURestData { get; private set; } = new();

        public async Task Stop()
        {
            if (!_isRunning)
            {
                return;
            }

            try
            {
                _cancellationTokenSource.Cancel();
                await _loopTask;
            }
            catch (OperationCanceledException)
            {
            }
            finally
            {
                _isRunning = false;
            }
        }

        private async Task LoopMethod(CancellationToken cancellationToken)
        {
            await Task.Delay(20000, cancellationToken); // Wait 20 seconds for rest data init or lmu ui can crash
            while (!_cancellationTokenSource.IsCancellationRequested)
            {
                try
                {
                    HttpResponseMessage response = await _client.GetAsync(NavigationUri, cancellationToken);
                    if (response.IsSuccessStatusCode && await IsInSession(response))
                    {
                        await LoadRestData(cancellationToken);
                    }
                }
                catch (TaskCanceledException)
                {
                }

                await Task.Delay(1000, cancellationToken);
            }
        }

        private async Task<bool> IsInSession(HttpResponseMessage response)
        {
            string content = await response.Content.ReadAsStringAsync();
            JObject resultParsed = JObject.Parse(content);
            JToken loadingInfo = resultParsed["loadingStatus"];

            if (loadingInfo?["loading"]?.ToObject<bool>() != false)
            {
                return false;
            }

            JToken state = resultParsed["state"];
            string navigationState = state?["navigationState"]?.ToObject<string>();
            return !invalidNavigationStates.Contains(navigationState);
        }

        private async Task LoadRestData(CancellationToken cancellationToken)
        {
            LmURestData lmURestData = new LmURestData();
            Task loadVirtualEnergyTask = LoadVirtualEnergyData(cancellationToken, lmURestData);
            Task loadDriversDataTask = LoadDriverData(cancellationToken, lmURestData);

            await Task.WhenAll(loadVirtualEnergyTask, loadDriversDataTask);

            LmURestData = lmURestData;
        }

        private async Task LoadVirtualEnergyData(CancellationToken cancellationToken, LmURestData lmURestData)
        {
            HttpResponseMessage response = await _client.GetAsync(GarageInfoUri, cancellationToken);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                JObject resultParsed = JObject.Parse(content);

                if (TryParseVirtualEnergyResult(resultParsed, out double currentVirtualEnergy, out double maxVirtualEnergy))
                {
                    lmURestData.CurrentVirtualEnergy = currentVirtualEnergy;
                    lmURestData.MaxVirtualEnergy = maxVirtualEnergy;
                }
            }
        }
        
        private async Task LoadDriverData(CancellationToken cancellationToken, LmURestData lmURestData)
        {
            HttpResponseMessage response = await _client.GetAsync(StandingsUri, cancellationToken);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                JObject resultParsed = JObject.Parse(content);

                if (TryParseDriverData(resultParsed, out List<LmURestDriverData> driversData))
                {
                    driversData.ForEach(x => lmURestData.DriversData[x.Position] = x);
                }
            }
        }
        
        private static bool TryParseDriverData(JObject result, out List<LmURestDriverData> driversData)
        {
            JArray vehiclesInOrder = result["standings"]["vehiclesInOrder"] as JArray;
            driversData = new List<LmURestDriverData>();

            if (vehiclesInOrder == null)
            {
                return false;
            }

            driversData = vehiclesInOrder.Select(x =>
            {
                JArray compounds = (JArray)x["compounds"];
                if (compounds == null || compounds.Count != 4)
                {
                    return new LmURestDriverData(-1, string.Empty, string.Empty, string.Empty, string.Empty);
                }

                int pos = x["pos"].ToObject<int>();
                string frontLeftCompound = compounds[0]["name"].ToObject<string>();
                string frontRightCompound = compounds[1]["name"].ToObject<string>();
                string rearLeftCompound = compounds[2]["name"].ToObject<string>();
                string rearRightCompound = compounds[3]["name"].ToObject<string>();
                return new LmURestDriverData(pos, frontLeftCompound, frontRightCompound, rearLeftCompound, rearRightCompound);
            }).ToList();

            return true;
        }

        private static bool TryParseVirtualEnergyResult(JObject result, out double currentVirtualEnergy, out double maxVirtualEnergy)
        {
            JToken fuelInfo = result["fuelInfo"];
            currentVirtualEnergy = -1;
            maxVirtualEnergy = -1;

            if (fuelInfo == null)
            {
                return false;
            }

            JValue currentVirtualEnergyJson = (JValue)fuelInfo["currentVirtualEnergy"];
            JValue maxVirtualEnergyJson = (JValue)fuelInfo["maxVirtualEnergy"];

            if (currentVirtualEnergyJson == null || maxVirtualEnergyJson == null)
            {
                return false;
            }

            currentVirtualEnergy = currentVirtualEnergyJson.ToObject<double>();
            maxVirtualEnergy = maxVirtualEnergyJson.ToObject<double>();
            return true;
        }
    }
}
