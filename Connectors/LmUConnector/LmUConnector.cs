﻿namespace SecondMonitor.LmUConnector
{
    using System;
    using System.IO;
    using System.Threading;
    using System.Threading.Tasks;

    using NLog;

    using SecondMonitor.DataModel.BasicProperties;
    using SecondMonitor.DataModel.Snapshot;
    using SecondMonitor.Foundation.Connectors;
    using SecondMonitor.Foundation.Connectors.DependencyChecker;
    using SecondMonitor.Foundation.Connectors.SharedMemory;
    using SecondMonitor.Foundation.Connectors.Visitor;
    using SecondMonitor.LmUConnector.SharedMemory;
    using SecondMonitor.LmUConnector.SharedMemory.rFactor2Data;

    // Based on https://github.com/TheIronWolfModding/rf2SharedMemoryMapPlugin
    public class LmUConnector : AbstractGameConnector
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static readonly string[] lmUExecutables = { "Le Mans Ultimate" };
        private readonly TimeSpan _connectionTimeout = TimeSpan.FromSeconds(120);
        private readonly LmUDataConverter _lmUDataConverter;
        private readonly MappedBuffer<lmUTelemetry> _telemetryBuffer = new MappedBuffer<lmUTelemetry>(rFactor2Constants.MM_TELEMETRY_FILE_NAME);
        private readonly MappedBuffer<lmUScoring> _scoringBuffer = new MappedBuffer<lmUScoring>(rFactor2Constants.MM_SCORING_FILE_NAME);
        private readonly MappedBuffer<lmURules> _rulesBuffer = new MappedBuffer<lmURules>(rFactor2Constants.MM_RULES_FILE_NAME);
        private readonly MappedBuffer<lmUExtended> _extendedBuffer = new MappedBuffer<lmUExtended>(rFactor2Constants.MM_EXTENDED_FILE_NAME);
        private readonly DependencyChecker _dependencies;
        private readonly SessionTimeInterpolator _sessionTimeInterpolator;
        private readonly LmuRestConnector _restConnector;

        private DateTime _connectionTime = DateTime.MinValue;
        private int _rawLastSessionType = int.MinValue;
        private SessionPhase _lastSessionPhase;
        private SessionType _lastSessionType;
        private bool _isConnected;
        private TimeSpan _previousSessionTime;

        public LmUConnector()
            : base(lmUExecutables)
        {
            _restConnector = new LmuRestConnector();
            TickTime = 16;
            _sessionTimeInterpolator = new SessionTimeInterpolator(TimeSpan.FromMilliseconds(190));
            _dependencies = new DependencyChecker(new FileExistDependency[]
            {
                new FileExistDependency(@"Plugins\rFactor2SharedMemoryMapPlugin64.dll", @"Connectors\RFactor2\rFactor2SharedMemoryMapPlugin64.dll"),
                new FileExistDependency(@"UserData\player\CustomPluginVariables.JSON", @"Connectors\RFactor2\CustomPluginVariables.JSON")
            }, () => true);
            _lmUDataConverter = new LmUDataConverter(_sessionTimeInterpolator);
        }

        public override bool IsConnected => _isConnected;

        protected override string ConnectorName => "LM Ultimate";

        protected override void OnConnection()
        {
            ResetConnector();
            CheckDependencies();
            if (_connectionTime == DateTime.MinValue)
            {
                _connectionTime = DateTime.Now;
            }

            if (DateTime.Now - _connectionTime > _connectionTimeout)
            {
                SendMessageToClients(
                    "Lm Ultimate has been detected running for extended time, but SecondMonitor wasn't able to connect to its shared memory.\n"
                    + "Please make sure that the rFactor2SharedMemoryMapPlugin64.dll is correctly installed in the plugins folder and enabled");
                _connectionTime = DateTime.MaxValue;
            }

            try
            {
                _telemetryBuffer.Connect();
                _scoringBuffer.Connect();
                _rulesBuffer.Connect();
                _extendedBuffer.Connect();
                _restConnector.Start();
                _isConnected = true;
            }
            catch (Exception)
            {
                Disconnect();
                throw;
            }
        }

        private void CheckDependencies()
        {
            try
            {
                if (Process != null && !_dependencies.Checked)
                {
                    string directory = Path.Combine(Path.GetPathRoot(Process.MainModule.FileName), Path.GetDirectoryName(Process.MainModule.FileName));
                    Action actionToInstall = _dependencies.CheckAndReturnInstallDependenciesAction(directory);
                    if (actionToInstall != null)
                    {
                        SendMessageToClients("Lm Ultimate has been detected, but the required plugin, rFactor2SharedMemoryMapPlugin64.dll, was not found. Do you want Second Monitor to install this plugin? You will need to restart the sim, after it is done.",
                            () => RunActionAndShowConfirmation(actionToInstall, "Plugin installed.\nBut it might be necessary go to UserData\\player\\CustomPluginVariables.JSON to enable the plugin there.", "Unable to install the plugin, unexpected error: "));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Unable to check dependencies.");
            }
        }

        private void RunActionAndShowConfirmation(Action actionToRun, string completionMessage, string errorMessage)
        {
            try
            {
                actionToRun();
                SendMessageToClients(completionMessage);
            }
            catch (Exception ex)
            {
                SendMessageToClients(errorMessage + "\n" + ex.Message);
            }
        }

        private void Disconnect()
        {
            _extendedBuffer.Disconnect();
            _scoringBuffer.Disconnect();
            _rulesBuffer.Disconnect();
            _telemetryBuffer.Disconnect();
            _isConnected = false;
        }

        protected override void ResetConnector()
        {
            _rawLastSessionType = int.MinValue;
            _lastSessionType = SessionType.Na;
            _lastSessionPhase = SessionPhase.Countdown;
            _previousSessionTime = TimeSpan.MinValue;
        }

        protected override async Task DaemonMethod(CancellationToken cancellationToken)
        {
            _connectionTime = DateTime.MinValue;
            while (!ShouldDisconnect)
            {
                await Task.Delay(TickTime, cancellationToken).ConfigureAwait(false);
                LmUFullData rFactorData = Load();
                SimulatorDataSet dataSet;
                try
                {
                    dataSet = _lmUDataConverter.CreateSimulatorDataSet(rFactorData, _restConnector.LmURestData);
                }
                catch (LMUInvalidPackageException ex)
                {
                    if (ex.InnerException != null && ex.InnerException is not LMUInvalidPackageException)
                    {
                        Logger.Error(ex, "LMUInvalid Package Caused by Exception");
                    }

                    continue;
                }

                if (CheckSessionStarted(rFactorData, dataSet))
                {
                    _sessionTimeInterpolator.Reset();
                    RaiseSessionStartedEvent(dataSet);
                }

                _previousSessionTime = dataSet.SessionInfo.SessionTime;
                RaiseDataLoadedEvent(dataSet);

                if (!IsProcessRunning())
                {
                    ShouldDisconnect = true;
                }
            }

            await _restConnector.Stop();
            Disconnect();
            RaiseDisconnectedEvent();
        }

        private LmUFullData Load()
        {
            if (!IsConnected)
            {
                throw new InvalidOperationException("Not connected");
            }

            LmUFullData data = new LmUFullData(_telemetryBuffer.GetMappedDataUnSynchronized(),
                _scoringBuffer.GetMappedDataUnSynchronized(),
                _rulesBuffer.GetMappedDataUnSynchronized(), _extendedBuffer.GetMappedDataUnSynchronized());
            return data;
        }

        private bool CheckSessionStarted(LmUFullData lmUData, SimulatorDataSet dataSet)
        {
            if (_previousSessionTime.TotalSeconds - 1 > dataSet.SessionInfo.SessionTime.TotalSeconds)
            {
                Logger.Info($"New Session - Session time is less. Oldtime : {_previousSessionTime.TotalSeconds} - New Time: {dataSet.SessionInfo.SessionTime.TotalSeconds}");
                return true;
            }

            if (_rawLastSessionType != lmUData.scoring.mScoringInfo.mSession || _lastSessionType != dataSet.SessionInfo.SessionType)
            {
                Logger.Info("New Session - Session type doesn't match");
                _lastSessionType = dataSet.SessionInfo.SessionType;
                _rawLastSessionType = lmUData.scoring.mScoringInfo.mSession;
                _lastSessionPhase = dataSet.SessionInfo.SessionPhase;
                return true;
            }

            if (dataSet.SessionInfo.SessionPhase != _lastSessionPhase && _lastSessionPhase != SessionPhase.Green && dataSet.SessionInfo.SessionPhase != SessionPhase.Countdown)
            {
                Logger.Info(
                    $"New Session - Session phase doesn't match. LastRawSessionPhase: {_rawLastSessionType}, CurrentSessionPhase: {lmUData.scoring.mScoringInfo.mSession} ");
                _lastSessionType = dataSet.SessionInfo.SessionType;
                _rawLastSessionType = lmUData.scoring.mScoringInfo.mSession;
                _lastSessionPhase = dataSet.SessionInfo.SessionPhase;
                return true;
            }

            return false;
        }
    }
}
