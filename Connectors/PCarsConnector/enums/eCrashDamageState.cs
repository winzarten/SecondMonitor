namespace SecondMonitor.PCarsConnector.Enums
{
    public enum ECrashDamageState
    {
        CrashDamageNone = 0,

        CrashDamageOfftrack,

        CrashDamageLargeProp,

        CrashDamageSpinning,

        CrashDamageRolling,

        // -------------
        CrashMax
    }
}
