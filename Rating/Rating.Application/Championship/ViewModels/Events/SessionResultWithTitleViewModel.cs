﻿namespace SecondMonitor.Rating.Application.Championship.ViewModels.Events
{
    using Common.DataModel.Championship;
    using Common.DataModel.Championship.Events;
    using SecondMonitor.ViewModels;
    using SecondMonitor.ViewModels.Factory;

    public class SessionResultWithTitleViewModel : AbstractViewModel<(ChampionshipDto Championship, EventDto EventDto, SessionDto SessionDto)>
    {
        private bool _showManufacturerResults;
        private string _remark;
        private bool _showTeamsResults;

        public SessionResultWithTitleViewModel(IViewModelFactory viewModelFactory)
        {
            EventTitleViewModel = viewModelFactory.Create<EventTitleViewModel>();
            DriversSessionResultViewModel = viewModelFactory.Create<DriversSessionResultViewModel>();
            TeamsSessionResultViewModel = viewModelFactory.Create<TeamsSessionResultViewModel>();
            ManufacturersSessionResultViewModel = viewModelFactory.Create<ManufacturersSessionResultViewModel>();
        }

        public EventTitleViewModel EventTitleViewModel { get; }
        public DriversSessionResultViewModel DriversSessionResultViewModel { get; }
        public TeamsSessionResultViewModel TeamsSessionResultViewModel { get; }
        public ManufacturersSessionResultViewModel ManufacturersSessionResultViewModel { get; }
        public string Remark
        {
            get => _remark;
            private set => SetProperty(ref _remark, value);
        }

        public bool ShowManufacturerResults
        {
            get => _showManufacturerResults;
            private set => SetProperty(ref _showManufacturerResults, value);
        }
        
        public bool ShowTeamsResults
        {
            get => _showTeamsResults;
            private set => SetProperty(ref _showTeamsResults, value);
        }

        protected override void ApplyModel((ChampionshipDto Championship, EventDto EventDto, SessionDto SessionDto) model)
        {
            (ChampionshipDto championship, EventDto eventDto, SessionDto sessionDto) = model;
            EventTitleViewModel.FromModel(model);
            DriversSessionResultViewModel.FromModel(sessionDto.SessionResult);

            TeamsSessionResultViewModel.FromModel(sessionDto.SessionResult);
            ShowTeamsResults = championship.IsTeamScoringEnabled && TeamsSessionResultViewModel.TeamsFinish.Count > 1;

            ManufacturersSessionResultViewModel.FromModel(sessionDto.SessionResult);
            ShowManufacturerResults = championship.IsManufacturerScoringEnabled && ManufacturersSessionResultViewModel.ManufacturerFinish.Count > 1;

            Remark = sessionDto.SessionResult.Remarks;
            if (Remark.Length > 200)
            {
                Remark = Remark.Substring(0, 200) + "...";
            }
        }

        public override (ChampionshipDto Championship, EventDto EventDto, SessionDto SessionDto) SaveToNewModel()
        {
            throw new System.NotSupportedException();
        }
    }
}