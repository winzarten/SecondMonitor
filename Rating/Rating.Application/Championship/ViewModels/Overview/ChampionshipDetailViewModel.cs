﻿namespace SecondMonitor.Rating.Application.Championship.ViewModels.Overview
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Input;
    using Common.DataModel.Championship;

    using Events;

    using SecondMonitor.Rating.Application.Championship.ViewModels.Overview.ProgressChart;
    using SecondMonitor.Rating.Application.Championship.ViewModels.Overview.ResultGrid;
    using SecondMonitor.ViewModels;
    using SecondMonitor.ViewModels.Factory;

    public class ChampionshipDetailViewModel : AbstractViewModel<ChampionshipDto>
    {
        private readonly IViewModelFactory _viewModelFactory;
        private readonly DriversStandingOverviewViewModel _driversStandingOverviewViewModel;
        private ICommand _closeCommand;

        public ChampionshipDetailViewModel(IViewModelFactory viewModelFactory)
        {
            _viewModelFactory = viewModelFactory;
            StandingsViewModels = new List<IViewModel>();
            _driversStandingOverviewViewModel = viewModelFactory.Create<DriversStandingOverviewViewModel>();
            CalendarResultsOverview = viewModelFactory.Create<CalendarResultsOverviewViewModel>();
            ChampionshipSessionsResults = viewModelFactory.Create<ChampionshipSessionsResultsViewModel>();
            DriversResultGridViewModel = viewModelFactory.Create<DriversResultGridViewModel>();
            ChampionshipProgressChartViewModel = viewModelFactory.Create<ChampionshipProgressChartViewModel>();
        }

        public string ChampionshipName { get; private set; }

        public List<IViewModel> StandingsViewModels { get; }

        public CalendarResultsOverviewViewModel CalendarResultsOverview { get; }

        public DriversResultGridViewModel DriversResultGridViewModel { get; }

        public ChampionshipSessionsResultsViewModel ChampionshipSessionsResults { get; }

        public ChampionshipProgressChartViewModel ChampionshipProgressChartViewModel { get; }

        public ICommand CloseCommand
        {
            get => _closeCommand;
            set => SetProperty(ref _closeCommand, value);
        }

        protected override void ApplyModel(ChampionshipDto model)
        {
            if (model == null)
            {
                return;
            }

            StandingsViewModels.Clear();

            ChampionshipName = model.ChampionshipName;
            _driversStandingOverviewViewModel.FromModel(model.Drivers);
            CalendarResultsOverview.FromModel(model);
            ChampionshipSessionsResults.FromModel(model);
            DriversResultGridViewModel.FromModel(model);
            ChampionshipProgressChartViewModel.FromModel(model);

            StandingsViewModels.Add(_driversStandingOverviewViewModel);

            foreach (TeamsChampionshipDto championshipDto in model.TeamsChampionshipDtos.Where(x => x.TeamDtos.Count > 1))
            {
                TeamsStandingOverviewViewModel resultViewModel = _viewModelFactory.Create<TeamsStandingOverviewViewModel>();
                resultViewModel.FromModel(championshipDto);
                StandingsViewModels.Add(resultViewModel);
            }

            foreach (ManufacturersChampionshipDto championshipDto in model.ManufacturersChampionshipDtos.Where(x => x.Manufacturers.Count > 1))
            {
                ManufacturersStandingOverviewViewModel resultViewModel = _viewModelFactory.Create<ManufacturersStandingOverviewViewModel>();
                resultViewModel.FromModel(championshipDto);
                StandingsViewModels.Add(resultViewModel);
            }
        }

        public override ChampionshipDto SaveToNewModel()
        {
            throw new NotSupportedException();
        }
    }
}