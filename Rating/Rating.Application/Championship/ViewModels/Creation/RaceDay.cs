﻿namespace SecondMonitor.Rating.Application.Championship.ViewModels.Creation
{
    public enum RaceDay
    {
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday,
        Sunday,
        Any,
        Random
    }
}
