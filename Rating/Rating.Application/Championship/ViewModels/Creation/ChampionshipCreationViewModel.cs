﻿namespace SecondMonitor.Rating.Application.Championship.ViewModels.Creation
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Linq;
    using System.Windows.Input;

    using SecondMonitor.Rating.Application.Championship.ViewModels.Creation.Calendar;
    using SecondMonitor.ViewModels;
    using SecondMonitor.ViewModels.Factory;
    using Session;

    public class ChampionshipCreationViewModel : AbstractViewModel
    {
        private bool _isSimulatorSelectionEnabled;
        private string _championshipTitle;
        private bool _aiNamesCanChange;
        private bool _isClassChangeEnabled;
        private bool _isMysteryClass;

        private bool _isOkButtonEnabled;
        private ICommand _okCommand;
        private ICommand _cancelCommand;
        private bool _isManufacturerScoringEnabled;
        private bool _isTeamScoringEnabled;
        private bool _isTeamScoringCheckBoxVisible;

        public ChampionshipCreationViewModel(IViewModelFactory viewModelFactory)
        {
            CalendarDefinitionViewModel = viewModelFactory.Create<CalendarDefinitionViewModel>();
            SessionsDefinitionViewModel = viewModelFactory.Create<SessionsDefinitionViewModel>();
            ClassesSelectionViewModel = viewModelFactory.Create<ClassesSelectionViewModel>();
            IsClassChangeEnabled = true;
            AiNamesCanChange = true;
            IsManufacturerScoringEnabled = true;
            IsTeamScoringEnabled = true;
            IsTeamScoringCheckBoxVisible = true;
            ChampionshipTitle = "Custom Championship";
            CalendarDefinitionViewModel.CalendarViewModel.PropertyChanged += CalendarViewModelOnPropertyChanged;
            CalendarDefinitionViewModel.CalendarViewModel.CalendarEntries.CollectionChanged += CalendarEntriesOnCollectionChanged;
        }

        public bool IsSimulatorSelectionEnabled
        {
            get => _isSimulatorSelectionEnabled;
            set => SetProperty(ref _isSimulatorSelectionEnabled, value);
        }

        public CalendarDefinitionViewModel CalendarDefinitionViewModel { get; }

        public SessionsDefinitionViewModel SessionsDefinitionViewModel { get; }

        public ClassesSelectionViewModel ClassesSelectionViewModel { get;  }

        public string[] AvailableSimulators { get; set; }

        public string SelectedSimulator { get; set; }

        public string ChampionshipTitle
        {
            get => _championshipTitle;
            set => SetProperty(ref _championshipTitle, value);
        }

        public ICommand ConfirmSimulatorCommand { get; set; }

        public bool AiNamesCanChange
        {
            get => _aiNamesCanChange;
            set => SetProperty(ref _aiNamesCanChange, value);
        }

        public bool IsManufacturerScoringEnabled
        {
            get => _isManufacturerScoringEnabled;
            set => SetProperty(ref _isManufacturerScoringEnabled, value);
        }

        public bool IsTeamScoringEnabled
        {
            get => _isTeamScoringEnabled;
            set => SetProperty(ref _isTeamScoringEnabled, value);
        }

        public bool IsTeamScoringCheckBoxVisible
        {
            get => _isTeamScoringCheckBoxVisible;
            set => SetProperty(ref _isTeamScoringCheckBoxVisible, value);
        }

        public bool IsOkButtonEnabled
        {
            get => _isOkButtonEnabled;
            set => SetProperty(ref _isOkButtonEnabled, value);
        }

        public ICommand OkCommand
        {
            get => _okCommand;
            set => SetProperty(ref _okCommand, value);
        }

        public ICommand CancelCommand
        {
            get => _cancelCommand;
            set => SetProperty(ref _cancelCommand, value);
        }

        public bool IsClassChangeEnabled
        {
            get => _isClassChangeEnabled;
            set => SetProperty(ref _isClassChangeEnabled, value);
        }

        public bool IsMysteryClass
        {
            get => _isMysteryClass;
            set => SetProperty(ref _isMysteryClass, value);
        }

        private void CalendarEntriesOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            ReevaluateIsOkButtonEnabled();
            UpdateTrackUsagesCount(e.NewItems ?? new List<ExistingTrackCalendarEntryViewModel>(), e.OldItems ?? new List<ExistingTrackCalendarEntryViewModel>());
        }

        private void UpdateTrackUsagesCount(IList addedItems, IList removedItems)
        {
            foreach (ExistingTrackCalendarEntryViewModel addedTrack in addedItems.OfType<ExistingTrackCalendarEntryViewModel>())
            {
                ExistingTrackTemplateViewModel trackTemplate = CalendarDefinitionViewModel.AvailableTracksViewModel.GetTrackByName(addedTrack.TrackName);
                if (trackTemplate != null)
                {
                    trackTemplate.UsageCount++;
                }
            }

            foreach (ExistingTrackCalendarEntryViewModel removedTrack in removedItems.OfType<ExistingTrackCalendarEntryViewModel>())
            {
                ExistingTrackTemplateViewModel trackTemplate = CalendarDefinitionViewModel.AvailableTracksViewModel.GetTrackByName(removedTrack.TrackName);
                if (trackTemplate != null)
                {
                    trackTemplate.UsageCount--;
                }
            }
        }

        private void CalendarViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            ReevaluateIsOkButtonEnabled();
        }

        private void ReevaluateIsOkButtonEnabled()
        {
            IsOkButtonEnabled = CalendarDefinitionViewModel.CalendarViewModel.CalendarEntries.Count > 0 && CalendarDefinitionViewModel.CalendarViewModel.StartOfCalendar < CalendarDefinitionViewModel.CalendarViewModel.EndOfCalendar;
        }
    }
}