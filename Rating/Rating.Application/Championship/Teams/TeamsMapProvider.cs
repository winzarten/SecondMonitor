﻿namespace SecondMonitor.Rating.Application.Championship.Teams
{
    using System;
    using System.Collections.Generic;

    public class TeamsMapProvider : ITeamsMapProvider
    {
        public TeamsMap<T> Map<T>(IEnumerable<T> entitiesToMap, Func<T, string> teamNameGetter)
        {
            Dictionary<string, List<T>> teamsMap = new();
            List<T> entitiesWithoutManufacturer = new();
            foreach (T entity in entitiesToMap)
            {
                string teamName = teamNameGetter(entity);
                if (string.IsNullOrWhiteSpace(teamName))
                {
                    entitiesWithoutManufacturer.Add(entity);
                    continue;
                }

                if (!teamsMap.TryGetValue(teamName, out List<T> entitiesOfManufacturer))
                {
                    entitiesOfManufacturer = new List<T>();
                    teamsMap.Add(teamName, entitiesOfManufacturer);
                }

                entitiesOfManufacturer.Add(entity);
            }

            return new TeamsMap<T>(teamsMap, entitiesWithoutManufacturer);
        }
    }
}
