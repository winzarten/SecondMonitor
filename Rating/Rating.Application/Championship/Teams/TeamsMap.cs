﻿namespace SecondMonitor.Rating.Application.Championship.Teams
{
    using System.Collections.Generic;

    public class TeamsMap<T> : ParticipantMap<string, T>
    {
        public TeamsMap(Dictionary<string, List<T>> map, List<T> entitiesWithUnknownParticipants) : base(map, entitiesWithUnknownParticipants)
        {
        }
    }
}
