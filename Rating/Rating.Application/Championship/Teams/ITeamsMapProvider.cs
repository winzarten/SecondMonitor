﻿namespace SecondMonitor.Rating.Application.Championship.Teams
{
    using System;
    using System.Collections.Generic;

    public interface ITeamsMapProvider
    {
        public TeamsMap<T> Map<T>(IEnumerable<T> entitiesToMap, Func<T, string> teamNameGetter);
    }
}