﻿namespace SecondMonitor.Rating.Application.Championship
{
    using System.Linq;
    using System.Windows;
    using Common.DataModel.Championship;
    using Common.DataModel.Championship.Events;
    using Contracts.Commands;
    using Contracts.TrackMap;
    using Contracts.TrackRecords;
    using DataModel.BasicProperties;
    using DataModel.Snapshot;
    using DataModel.TrackMap;
    using DataModel.TrackRecords;

    using SecondMonitor.Rating.Application.Championship.ViewModels.Overview.ProgressChart;
    using SecondMonitor.Rating.Application.Championship.ViewModels.Overview.ResultGrid;
    using SecondMonitor.ViewModels;
    using SecondMonitor.ViewModels.Factory;
    using SecondMonitor.ViewModels.Settings;
    using ViewModels.Events;
    using ViewModels.Overview;

    public class ChampionshipDialogProvider : IChampionshipDialogProvider
    {
        private readonly IWindowService _windowService;
        private readonly IViewModelFactory _viewModelFactory;
        private readonly ITrackRecordsProvider _trackRecordsProvider;
        private readonly ISettingsProvider _settingsProvider;
        private readonly IMapsLoader _mapsLoader;

        public ChampionshipDialogProvider(IWindowService windowService, IViewModelFactory viewModelFactory, ITrackRecordsProvider trackRecordsProvider, IMapsLoaderFactory mapsLoaderFactory, ISettingsProvider settingsProvider)
        {
            _windowService = windowService;
            _viewModelFactory = viewModelFactory;
            _trackRecordsProvider = trackRecordsProvider;
            _settingsProvider = settingsProvider;
            _mapsLoader = mapsLoaderFactory.Create();
        }

        public void ShowWelcomeScreen(SimulatorDataSet dataSet, ChampionshipDto championship)
        {
            EventStartingViewModel eventStartingViewModel = _viewModelFactory.Create<EventStartingViewModel>();

            EventDto currentEvent = championship.GetCurrentOrLastEvent();
            SessionDto currentSession = currentEvent.Sessions[championship.CurrentSessionIndex];
            eventStartingViewModel.EventTitleViewModel.FromModel((championship, currentEvent, currentSession));

            eventStartingViewModel.Screens.Add(CreateTrackOverviewViewModel(dataSet, championship));
            eventStartingViewModel.Screens.Add(CreateDriversStandingOverviewViewModel(championship));

            ManufacturersChampionshipDto manufacturersChampionshipDto = championship.GetOrCreateManufacturerChampionship(dataSet.PlayerInfo.CarClassName, dataSet.PlayerInfo.CarClassId);

            if (championship.IsManufacturerScoringEnabled && manufacturersChampionshipDto.Manufacturers.Count() > 2)
            {
                eventStartingViewModel.Screens.Add(CreateManufacturersStandingOverviewViewModel(manufacturersChampionshipDto));
            }

            Window window = _windowService.OpenWindow(eventStartingViewModel, "Event Starting", WindowState.Maximized, SizeToContent.Manual, WindowStartupLocation.CenterOwner);
            eventStartingViewModel.CloseCommand = new RelayCommand(() => CloseWindow(window));
        }

        public void ShowLastEvenResultWindow(ChampionshipDto championship)
        {
            SessionCompletedViewModel sessionCompletedViewmodel = _viewModelFactory.Create<SessionCompletedViewModel>();
            (EventDto eventDto, SessionDto sessionDto) = championship.GetLastSessionWithResults();
            SessionResultDto lastResult = sessionDto.SessionResult;
            if (lastResult == null)
            {
                return;
            }

            sessionCompletedViewmodel.Title = "Session Completed";

            PodiumViewModel podiumViewModel = _viewModelFactory.Create<PodiumViewModel>();
            podiumViewModel.FromModel(lastResult);

            DriversSessionResultViewModel driversFinishViewModel = _viewModelFactory.Create<DriversSessionResultViewModel>();
            driversFinishViewModel.Header = "Session Driver Results";
            driversFinishViewModel.FromModel(lastResult);

            DriversNewStandingsViewModel driversNewStandingsViewModel = _viewModelFactory.Create<DriversNewStandingsViewModel>();
            driversNewStandingsViewModel.EventTitleViewModel.FromModel((championship, eventDto, sessionDto));
            driversNewStandingsViewModel.EventTitleViewModel.ChampionshipName = "Current Drivers Standings";
            driversNewStandingsViewModel.FromModel(lastResult);

            sessionCompletedViewmodel.Screens.Add(podiumViewModel);
            sessionCompletedViewmodel.Screens.Add(driversFinishViewModel);
            sessionCompletedViewmodel.Screens.Add(driversNewStandingsViewModel);

            AddTeamsResults(championship, lastResult, eventDto, sessionDto, sessionCompletedViewmodel);
            AddManufacturingResults(championship, lastResult, eventDto, sessionDto, sessionCompletedViewmodel);

            if (championship.ChampionshipState == ChampionshipState.Finished)
            {
                DriverDto player = championship.Drivers.First(x => x.IsPlayer);
                if (player.Position <= 3)
                {
                    TrophyViewModel trophyViewModel = _viewModelFactory.Create<TrophyViewModel>();
                    trophyViewModel.Position = player.Position;
                    trophyViewModel.DriverName = player.LastUsedName;
                    sessionCompletedViewmodel.Screens.Add(trophyViewModel);
                }

                DriversStandingOverviewViewModel championshipFinalStandings = _viewModelFactory.Create<DriversStandingOverviewViewModel>();
                championshipFinalStandings.Header = "Final Driver Standing: ";
                championshipFinalStandings.FromModel(championship.Drivers.OrderBy(x => x.Position));
                sessionCompletedViewmodel.Screens.Add(championshipFinalStandings);

                DriversResultGridViewModel driversResultGridViewModel = _viewModelFactory.Create<DriversResultGridViewModel>();
                driversResultGridViewModel.FromModel(championship);
                sessionCompletedViewmodel.Screens.Add(driversResultGridViewModel);

                ChampionshipProgressChartViewModel championshipProgressChartViewModel = _viewModelFactory.Create<ChampionshipProgressChartViewModel>();
                championshipProgressChartViewModel.FromModel(championship);
                sessionCompletedViewmodel.Screens.Add(championshipProgressChartViewModel);

                AddTeamsFinalStandings(championship, sessionCompletedViewmodel);
                AddManufacturerFinalStandings(championship, sessionCompletedViewmodel);
            }

            Window window = _windowService.OpenWindow(sessionCompletedViewmodel, "Session Completed", WindowState.Maximized, SizeToContent.Manual, WindowStartupLocation.CenterOwner);
            sessionCompletedViewmodel.CloseCommand = new RelayCommand(() => CloseWindow(window));
        }

        private void AddManufacturingResults(ChampionshipDto championship, SessionResultDto lastResult,
            EventDto eventDto, SessionDto sessionDto, SessionCompletedViewModel sessionCompletedViewmodel)
        {
            ManufacturersChampionshipDto manufacturersChampionshipDto = championship.GetOrCreateManufacturerChampionship();
            if (!championship.IsManufacturerScoringEnabled || manufacturersChampionshipDto.Manufacturers.Count <= 1)
            {
                return;
            }

            ManufacturersSessionResultViewModel manufacturerResultViewModel = _viewModelFactory.Create<ManufacturersSessionResultViewModel>();
            manufacturerResultViewModel.Header = $"Session Manufacturers Results ({manufacturersChampionshipDto.ClassName})";
            manufacturerResultViewModel.FromModel(lastResult);

            ManufacturerNewStandingsViewModel manufacturerNewStandingsViewModel = _viewModelFactory.Create<ManufacturerNewStandingsViewModel>();
            manufacturerNewStandingsViewModel.EventTitleViewModel.FromModel((championship, eventDto, sessionDto));
            manufacturerNewStandingsViewModel.EventTitleViewModel.ChampionshipName = $"Current Manufacturers Standings ({manufacturersChampionshipDto.ClassName})";
            manufacturerNewStandingsViewModel.FromModel(lastResult);

            sessionCompletedViewmodel.Screens.Add(manufacturerResultViewModel);
            sessionCompletedViewmodel.Screens.Add(manufacturerNewStandingsViewModel);
        }

        private void AddTeamsResults(ChampionshipDto championship, SessionResultDto lastResult,
            EventDto eventDto, SessionDto sessionDto, SessionCompletedViewModel sessionCompletedViewmodel)
        {
            TeamsChampionshipDto teamsChampionshipDto = championship.GetOrCreateTeamChampionship();
            if (!championship.HasTeamsChampionships)
            {
                return;
            }

            TeamsSessionResultViewModel teamResultViewModel = _viewModelFactory.Create<TeamsSessionResultViewModel>();
            teamResultViewModel.Header = $"Session Team Results ({teamsChampionshipDto.ClassName})";
            teamResultViewModel.FromModel(lastResult);

            TeamsNewStandingsViewModel teamsNewStandingsViewModel = _viewModelFactory.Create<TeamsNewStandingsViewModel>();
            teamsNewStandingsViewModel.EventTitleViewModel.FromModel((championship, eventDto, sessionDto));
            teamsNewStandingsViewModel.EventTitleViewModel.ChampionshipName = $"Current Teams Standings ({teamsChampionshipDto.ClassName})";
            teamsNewStandingsViewModel.FromModel(lastResult);

            sessionCompletedViewmodel.Screens.Add(teamResultViewModel);
            sessionCompletedViewmodel.Screens.Add(teamsNewStandingsViewModel);
        }

        private void AddManufacturerFinalStandings(ChampionshipDto championship, SessionCompletedViewModel sessionCompletedViewmodel)
        {
            if (!championship.IsManufacturerScoringEnabled)
            {
                return;
            }

            foreach (ManufacturersChampionshipDto manufacturerChampionship in championship.ManufacturersChampionshipDtos.Where(x => x.Manufacturers.Count > 1))
            {
                ManufacturerDto playerManufacturer = manufacturerChampionship.Manufacturers.FirstOrDefault(x => x.IsPlayer);
                if (playerManufacturer is { Position: <= 3 } && playerManufacturer.Position < manufacturerChampionship.Manufacturers.Count)
                {
                    TrophyViewModel trophyViewModel = _viewModelFactory.Create<TrophyViewModel>();
                    trophyViewModel.Position = playerManufacturer.Position;
                    trophyViewModel.DriverName = playerManufacturer.ManufacturerName;
                    sessionCompletedViewmodel.Screens.Add(trophyViewModel);
                }

                ManufacturersStandingOverviewViewModel manufacturersFinalStandings = _viewModelFactory.Create<ManufacturersStandingOverviewViewModel>();
                manufacturersFinalStandings.FromModel(manufacturerChampionship);
                manufacturersFinalStandings.Header = $"Final Manufacturers Standing ({manufacturerChampionship.ClassName}): ";
                sessionCompletedViewmodel.Screens.Add(manufacturersFinalStandings);
            }
        }
        
        private void AddTeamsFinalStandings(ChampionshipDto championship, SessionCompletedViewModel sessionCompletedViewmodel)
        {
            if (!championship.IsTeamScoringEnabled)
            {
                return;
            }

            foreach (TeamsChampionshipDto teamsChampionship in championship.TeamsChampionshipDtos.Where(x => x.TeamDtos.Count > 1))
            {
                TeamDto playerTeam = teamsChampionship.TeamDtos.FirstOrDefault(x => x.IsPlayer);
                if (playerTeam is { Position: <= 3 } && playerTeam.Position < teamsChampionship.TeamDtos.Count)
                {
                    TrophyViewModel trophyViewModel = _viewModelFactory.Create<TrophyViewModel>();
                    trophyViewModel.Position = playerTeam.Position;
                    trophyViewModel.DriverName = playerTeam.TeamName;
                    sessionCompletedViewmodel.Screens.Add(trophyViewModel);
                }

                TeamsStandingOverviewViewModel manufacturersFinalStandings = _viewModelFactory.Create<TeamsStandingOverviewViewModel>();
                manufacturersFinalStandings.FromModel(teamsChampionship);
                manufacturersFinalStandings.Header = $"Final Teams Standing ({teamsChampionship.ClassName}): ";
                sessionCompletedViewmodel.Screens.Add(manufacturersFinalStandings);
            }
        }

        private TrackOverviewViewModel CreateTrackOverviewViewModel(SimulatorDataSet dataSet, ChampionshipDto championship)
        {
            TrackInfo trackInfo = dataSet.SessionInfo.TrackInfo;
            bool hasMap = _mapsLoader.TryLoadMap(championship.SimulatorName, trackInfo.TrackFullName, out TrackMapDto trackMapDto);
            TrackOverviewViewModel trackOverviewViewModel = _viewModelFactory.Create<TrackOverviewViewModel>();
            trackOverviewViewModel.TrackName = trackInfo.TrackFullName;
            trackOverviewViewModel.LayoutLength = $"{trackInfo.LayoutLength.GetByUnit(_settingsProvider.DisplaySettingsViewModel.DistanceUnits):N2} {Distance.GetUnitsSymbol(_settingsProvider.DisplaySettingsViewModel.DistanceUnits)} ";
            if (hasMap)
            {
                trackOverviewViewModel.TrackGeometryViewModel.FromModel(trackMapDto.TrackGeometry);
            }

            if (_trackRecordsProvider.TryGetOverallBestRecord(dataSet.Source, trackInfo.TrackFullName, SessionType.Race, out RecordEntryDto recordEntry))
            {
                trackOverviewViewModel.OverallRecord.FromModel(recordEntry);
            }

            if (_trackRecordsProvider.TryGetCarBestRecord(dataSet.Source, trackInfo.TrackFullName, dataSet.PlayerInfo.CarName, SessionType.Race, out recordEntry))
            {
                trackOverviewViewModel.CarRecord.FromModel(recordEntry);
            }

            if (_trackRecordsProvider.TryGetClassBestRecord(dataSet.Source, trackInfo.TrackFullName, dataSet.PlayerInfo.CarClassName, SessionType.Race, out recordEntry))
            {
                trackOverviewViewModel.ClassRecord.FromModel(recordEntry);
            }

            return trackOverviewViewModel;
        }

        public void OpenChampionshipDetailsWindow(ChampionshipDto championship)
        {
            ChampionshipDetailViewModel detailViewModel = _viewModelFactory.Create<ChampionshipDetailViewModel>();
            detailViewModel.FromModel(championship);
            Window window = _windowService.OpenWindow(detailViewModel, "Championships Details", WindowState.Maximized, SizeToContent.Manual, WindowStartupLocation.CenterOwner);
            detailViewModel.CloseCommand = new RelayCommand(() => CloseWindow(window));
            detailViewModel.ChampionshipSessionsResults.CloseCommand = new RelayCommand(() => CloseWindow(window));
        }

        private DriversStandingOverviewViewModel CreateDriversStandingOverviewViewModel(ChampionshipDto championship)
        {
            DriversStandingOverviewViewModel standingOverviewViewModel = _viewModelFactory.Create<DriversStandingOverviewViewModel>();
            standingOverviewViewModel.FromModel(championship.Drivers);
            return standingOverviewViewModel;
        }

        private ManufacturersStandingOverviewViewModel CreateManufacturersStandingOverviewViewModel(ManufacturersChampionshipDto championship)
        {
            ManufacturersStandingOverviewViewModel standingOverviewViewModel = _viewModelFactory.Create<ManufacturersStandingOverviewViewModel>();
            standingOverviewViewModel.FromModel(championship);
            return standingOverviewViewModel;
        }

        private void CloseWindow(Window window)
        {
            window.Close();
        }
    }
}