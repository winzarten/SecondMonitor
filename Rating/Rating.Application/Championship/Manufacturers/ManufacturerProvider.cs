﻿namespace SecondMonitor.Rating.Application.Championship.Manufacturers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class ManufacturerProvider : IManufacturerProvider
    {
        private readonly IReadOnlyCollection<Manufacturer> _allManufacturers = AllManufacturers.Manufacturers;

        public bool TryGetManufacturer(string carName, out Manufacturer manufacturer)
        {
            manufacturer = _allManufacturers.Where(x => x.IsManufacturerOf(carName))
                .OrderByDescending(x => x.Name.Length).FirstOrDefault();

            return manufacturer != null;
        }

        public ManufacturerMap<T> Map<T>(IEnumerable<T> entitiesToMap, Func<T, string> carNameGetter)
        {
            Dictionary<Manufacturer, List<T>> manufacturerMap = new();
            List<T> entitiesWithoutManufacturer = new();
            foreach (T entity in entitiesToMap)
            {
                if (!TryGetManufacturer(carNameGetter(entity), out Manufacturer manufacturer))
                {
                    entitiesWithoutManufacturer.Add(entity);
                    continue;
                }

                if (!manufacturerMap.TryGetValue(manufacturer, out List<T> entitiesOfManufacturer))
                {
                    entitiesOfManufacturer = new List<T>();
                    manufacturerMap.Add(manufacturer, entitiesOfManufacturer);
                }

                entitiesOfManufacturer.Add(entity);
            }

            return new ManufacturerMap<T>(manufacturerMap, entitiesWithoutManufacturer);
        }
    }
}