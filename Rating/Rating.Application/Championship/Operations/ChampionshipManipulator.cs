﻿namespace SecondMonitor.Rating.Application.Championship.Operations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Common.DataModel.Championship;
    using Common.DataModel.Championship.Events;

    using DataModel.BasicProperties;
    using DataModel.Snapshot;
    using DataModel.Snapshot.Drivers;

    using Manufacturers;

    using NLog;

    using SecondMonitor.DataModel.Extensions;
    using SecondMonitor.DataModel.SimulatorContent;
    using SecondMonitor.Rating.Application.Championship.Teams;
    using SecondMonitor.ViewModels.Settings;
    using SecondMonitor.ViewModels.Settings.ViewModel;
    using SecondMonitor.ViewModels.SimulatorContent;

    using Timing.Common.SessionTiming.Drivers.Lap;

    public class ChampionshipManipulator : IChampionshipManipulator
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly ISimulatorContentProvider _simulatorContentProvider;
        private readonly IManufacturerProvider _manufacturerProvider;
        private readonly ITeamsMapProvider _teamsMapProvider;
        private readonly Random _random;
        private readonly DisplaySettingsViewModel _displaySettingsViewModel;

        public ChampionshipManipulator(ISimulatorContentProvider simulatorContentProvider, 
            IManufacturerProvider manufacturerProvider,
            ITeamsMapProvider teamsMapProvider,
            ISettingsProvider settingsProvider)
        {
            _random = new Random();
            _simulatorContentProvider = simulatorContentProvider;
            _manufacturerProvider = manufacturerProvider;
            _teamsMapProvider = teamsMapProvider;
            _displaySettingsViewModel = settingsProvider.DisplaySettingsViewModel;
        }

        public void StartChampionship(ChampionshipDto championship, SimulatorDataSet dataSet)
        {
            InitializeDrivers(championship, dataSet);
            championship.GetCurrentOrLastEvent().EventStatus = EventStatus.InProgress;
            championship.ChampionshipState = ChampionshipState.Started;
        }

        public void StartNextEvent(ChampionshipDto championship, SimulatorDataSet dataSet)
        {
            EventDto currentEvent = championship.GetCurrentOrLastEvent();
            currentEvent.TrackName = dataSet.SessionInfo.TrackInfo.TrackFullName;
            championship.ClassName = dataSet.PlayerInfo.CarClassName;
        }

        public void AddResultsForCurrentSession(ChampionshipDto championship, SimulatorDataSet dataSet, ILapInfo bestLap, double completionPercentage)
        {
            UpdateDriversProperties(championship, dataSet);
            EventDto currentEvent = championship.GetCurrentOrLastEvent();
            SessionDto currentSession = currentEvent.Sessions[championship.CurrentSessionIndex];
            //Clear session result, so it is not used for drivers comparison
            currentSession.SessionResult = null;
            try
            {
                currentSession.SessionResult = CreateResultDto(championship, dataSet, bestLap, completionPercentage);
            }
            catch (InvalidOperationException ex)
            {
                Logger.Error(ex);
            }
        }

        public void UpdateDriversProperties(ChampionshipDto championship, SimulatorDataSet dataSet)
        {
            UpdatePlayerCar(championship, dataSet);
            UpdateAiDriversNames(championship, dataSet);
        }

        public void UpdatePlayerCar(ChampionshipDto championship, SimulatorDataSet dataSet)
        {
            DriverDto player = championship.Drivers.FirstOrDefault(x => x.IsPlayer);
            if (player == null || dataSet.PlayerInfo == null)
            {
                return;
            }

            player.LastCarName = dataSet.PlayerInfo.CarDisplayName;
            player.LastClassId = dataSet.PlayerInfo.CarClassId;
            player.LastClassName = dataSet.PlayerInfo.CarClassName;
            player.TeamName = dataSet.PlayerInfo.TeamName;

            SetPlayerManufacturer(championship, player);
            SetPlayerTeam(championship, player);
        }

        private void SetPlayerManufacturer(ChampionshipDto championship, DriverDto player)
        {
            if (!championship.IsManufacturerScoringEnabled)
            {
                return;
            }

            ManufacturersChampionshipDto manufacturersChampionshipDto = championship.GetOrCreateManufacturerChampionship();

            bool isPlayerManufacturerKnown = _manufacturerProvider.TryGetManufacturer(player.LastCarName, out Manufacturer playerManufacturer);
            manufacturersChampionshipDto.Manufacturers.ForEach(x => x.IsPlayer = false);
            if (isPlayerManufacturerKnown)
            {
                manufacturersChampionshipDto.GetOrCreateManufacturer(playerManufacturer.Name).IsPlayer = true;
            }
        }

        private void SetPlayerTeam(ChampionshipDto championship, DriverDto player)
        {
            if (!championship.IsTeamScoringEnabled)
            {
                return;
            }

            TeamsChampionshipDto manufacturersChampionshipDto = championship.GetOrCreateTeamChampionship();

            bool isPlayerManufacturerKnown = !string.IsNullOrWhiteSpace(player.TeamName);
            manufacturersChampionshipDto.TeamDtos.ForEach(x => x.IsPlayer = false);
            if (isPlayerManufacturerKnown)
            {
                manufacturersChampionshipDto.GetOrCreateTeam(player.TeamName).IsPlayer = true;
            }
        }

        public void UpdateAiDriversNames(ChampionshipDto championship, SimulatorDataSet dataSet)
        {
            Logger.Info("Championships - Updating Driver Names");
            DriverDto player = championship.Drivers.FirstOrDefault(x => x.IsPlayer);
            if (player != null && dataSet.PlayerInfo != null)
            {
                player.LastUsedName = dataSet.PlayerInfo.DriverLongName;
            }

            List<DriverDto> driverPool = championship.Drivers.Where(x => !x.IsPlayer).ToList();
            List<DriverInfo> driversToAssign = dataSet.GetDriversInfoByMultiClass().Where(x => !x.IsPlayer).ToList();

            List<DriverDto> driversByPosition = driverPool.OrderByDescending(x => x.Position).ToList();

            // If there are more drivers in the championships, then in session, then disable those drivers in championships that have the lowest score, until number of participants is equal
            // This is to avoid loosing high scoring drivers from championship, who are not present in the session. Their points are rather assigned to another driver
            for (int i = 0; i < driversByPosition.Count && driversToAssign.Count < driverPool.Count; i++)
            {
                driversByPosition[i].IsInactive = true;
                driverPool.Remove(driversByPosition[i]);
            }

            //First iteration = use first names
            foreach (DriverInfo driver in driversToAssign.ToList())
            {
                DriverDto driverMatch = driverPool.FirstOrDefault(x => x.FirstUsedName == driver.DriverLongName);
                if (driverMatch == null)
                {
                    continue;
                }

                Logger.Info($"Championships - Driver {driverMatch.FirstUsedName} matched by First Used Name");
                driverMatch.IsInactive = false;
                driverMatch.SetAnotherName(driver.DriverLongName);
                driverMatch.UpdateCarAndClass(driver);
                driverPool.Remove(driverMatch);
                driversToAssign.Remove(driver);
            }

            //2nd iteration = use previous names
            foreach (DriverInfo driver in driversToAssign.ToList())
            {
                DriverDto driverMatch = driverPool.FirstOrDefault(x => x.LastUsedName == driver.DriverLongName);
                if (driverMatch == null)
                {
                    continue;
                }

                Logger.Info($"Championships - Driver {driverMatch.LastUsedName} matched by Last Used Name");
                driverMatch.IsInactive = false;
                driverMatch.UpdateCarAndClass(driver);
                driverPool.Remove(driverMatch);
                driversToAssign.Remove(driver);
            }

            //3rd iteration, try to find drivers that previously used that name
            foreach (DriverInfo driver in driversToAssign.ToList())
            {
                DriverDto driverMatch = driverPool.FirstOrDefault(x => x.OtherNames.Contains(driver.DriverLongName));
                if (driverMatch == null)
                {
                    continue;
                }

                Logger.Info($"Championships - Driver {driver.DriverLongName} replaced {driverMatch.LastUsedName}");
                driverMatch.IsInactive = false;
                driverMatch.SetAnotherName(driver.DriverLongName);
                driverMatch.UpdateCarAndClass(driver);
                driverPool.Remove(driverMatch);
                driversToAssign.Remove(driver);
            }

            //4th iteration, try to find drivers that used the same car
            foreach (DriverInfo driver in driversToAssign.ToList())
            {
                DriverDto driverMatch = driverPool.FirstOrDefault(x => x.LastCarName == driver.CarDisplayName);
                if (driverMatch == null)
                {
                    continue;
                }

                Logger.Info($"Championships - Driver {driver.DriverLongName} replaced {driverMatch.LastUsedName}");
                driverMatch.IsInactive = false;
                driverMatch.SetAnotherName(driver.DriverLongName);
                driverMatch.UpdateCarAndClass(driver);
                driverPool.Remove(driverMatch);
                driversToAssign.Remove(driver);
            }

            while (driverPool.Count > 0)
            {
                if (driversToAssign.Count > 0)
                {
                    Logger.Info($"Championships - Driver {driversToAssign[0].DriverLongName} randomly replaced {driverPool[0].LastUsedName}");
                    driverPool[0].UpdateCarAndClass(driversToAssign[0]);
                    driverPool[0].SetAnotherName(driversToAssign[0].DriverLongName);
                    driverPool.RemoveAt(0);
                    driversToAssign.RemoveAt(0);
                    continue;
                }

                Logger.Info($"Championships - Driver {driverPool[0].LastUsedName} set as inactive");
                driverPool[0].IsInactive = true;
                driverPool.RemoveAt(0);
            }

            int position = championship.TotalDrivers;

            List<DriverDto> newDrives = driversToAssign.Select(x => new DriverDto()
            {
                LastUsedName = x.DriverLongName,
                FirstUsedName = x.DriverLongName,
                IsPlayer = false,
                Position = ++position,
                LastCarName = x.CarDisplayName,
                LastClassId = x.CarClassId,
                LastClassName = x.CarClassName,
                TeamName = x.TeamName,
            }).ToList();

            championship.Drivers.AddRange(newDrives);
            championship.TotalDrivers = championship.Drivers.Count(x => !x.IsInactive);

            UpdateResultsName(championship);
        }

        public void DnfPlayerAndCommitLastSessionResults(ChampionshipDto championship)
        {
            EventDto currentEvent = championship.GetCurrentOrLastEvent();
            SessionDto currentSession = currentEvent.Sessions[championship.CurrentSessionIndex];
            Dictionary<Guid, DriverDto> guidDriverDictionary = championship.GetGuidToDriverDictionary();

            if (currentSession.SessionResult == null)
            {
                Logger.Warn($"No Results for Championship: {championship.ChampionshipName}, advancing");
                AdvanceChampionship(championship);
                return;
            }

            SessionResultDto sessionResults = currentSession.SessionResult;

            ScoringDto scoring = championship.Scoring[championship.CurrentSessionIndex];

            DriverSessionResultDto playerResult = sessionResults.DriverSessionResult.Single(x => x.IsPlayer);
            int playerFinishingPosition = playerResult.FinishPosition;

            //Apply new scoring
            foreach (DriverSessionResultDto driverSessionResult in sessionResults.DriverSessionResult)
            {
                driverSessionResult.TotalPoints -= driverSessionResult.PointsGain;
                if (driverSessionResult.FinishPosition >= playerFinishingPosition)
                {
                    driverSessionResult.FinishPosition = driverSessionResult.IsPlayer ? sessionResults.DriverSessionResult.Count : driverSessionResult.FinishPosition - 1;
                }

                driverSessionResult.PointsGain = driverSessionResult.FinishPosition <= scoring.Scoring.Count ? scoring.Scoring[driverSessionResult.FinishPosition - 1] : 0;

                if (driverSessionResult.WasFastestLap && driverSessionResult.FinishPosition <= scoring.MaxPositionForFastestLap)
                {
                    driverSessionResult.PointsGain += scoring.PointsForFastestLap;
                }

                driverSessionResult.TotalPoints += driverSessionResult.PointsGain;
            }

            DriverSessionResultComparer comparer = new(championship, sessionResults);
            List<DriverSessionResultDto> resultsOrdered = sessionResults.DriverSessionResult.OrderBy(x => x, comparer).ToList();
            for (int i = 0; i < resultsOrdered.Count; i++)
            {
                resultsOrdered[i].AfterEventPosition = i + 1;
            }

            foreach (DriverSessionResultDto driverSessionResultDto in currentSession.SessionResult.DriverSessionResult)
            {
                DriverDto driverDto = guidDriverDictionary[driverSessionResultDto.DriverGuid];
                driverDto.TotalPoints = driverSessionResultDto.TotalPoints;
                driverDto.Position = driverSessionResultDto.AfterEventPosition;
                driverDto.LastCarName = driverSessionResultDto.CarName;
                driverDto.LastClassName = driverSessionResultDto.ClassName;
                driverDto.LastClassId = driverSessionResultDto.ClassId;
                driverDto.TeamName = driverSessionResultDto.TeamName;
            }

            sessionResults.Remarks = string.Empty;

            AddTeamsResults(championship, sessionResults);
            AddManufacturerResults(championship, sessionResults);
            CommitManufacturerResults(championship.GetOrCreateManufacturerChampionship(), sessionResults);
            CommitTeamsResults(championship.GetOrCreateTeamChampionship(), sessionResults);
            AdvanceChampionship(championship);
        }

        private void CommitManufacturerResults(ManufacturersChampionshipDto championship, SessionResultDto sessionResults)
        {
            foreach (ManufacturerSessionResultDto manufacturerSessionResult in sessionResults.ManufacturersSessionResult)
            {
                ManufacturerDto manufacturerDto = championship.GetOrCreateManufacturer(manufacturerSessionResult.ManufacturerName);
                manufacturerDto.TotalPoints = manufacturerSessionResult.TotalPoints;
                manufacturerDto.Position = manufacturerSessionResult.AfterEventPosition;
            }
        }

        private void CommitTeamsResults(TeamsChampionshipDto championship, SessionResultDto sessionResults)
        {
            foreach (TeamSessionResultDto teamSessionResultDto in sessionResults.TeamSessionResult)
            {
                TeamDto teamDto = championship.GetOrCreateTeam(teamSessionResultDto.TeamName);
                teamDto.TotalPoints = teamSessionResultDto.TotalPoints;
                teamDto.Position = teamSessionResultDto.AfterEventPosition;
            }
        }

        public void CommitLastSessionResults(ChampionshipDto championship)
        {
            EventDto currentEvent = championship.GetCurrentOrLastEvent();
            SessionDto currentSession = currentEvent.Sessions[championship.CurrentSessionIndex];
            Dictionary<Guid, DriverDto> guidDriverDictionary = championship.GetGuidToDriverDictionary();

            if (currentSession.SessionResult == null)
            {
                Logger.Warn($"No Results for Championship: {championship.ChampionshipName}, advancing");
                AdvanceChampionship(championship);
                return;
            }

            foreach (DriverSessionResultDto driverSessionResultDto in currentSession.SessionResult.DriverSessionResult)
            {
                DriverDto driverDto = guidDriverDictionary[driverSessionResultDto.DriverGuid];
                driverDto.TotalPoints = driverSessionResultDto.TotalPoints;
                driverDto.Position = driverSessionResultDto.AfterEventPosition;
                driverDto.LastCarName = driverSessionResultDto.CarName;
                driverDto.LastClassName = driverSessionResultDto.ClassName;
                driverDto.LastClassId = driverSessionResultDto.ClassId;
                driverDto.TeamName = driverSessionResultDto.TeamName;
            }

            CommitManufacturerResults(championship.GetOrCreateManufacturerChampionship(), currentSession.SessionResult);
            CommitTeamsResults(championship.GetOrCreateTeamChampionship(), currentSession.SessionResult);
            AdvanceChampionship(championship);
        }

        private void UpdateResultsName(ChampionshipDto championship)
        {
            Dictionary<Guid, DriverDto> driverDictionary = championship.GetGuidToDriverDictionary();
            foreach (DriverSessionResultDto driverResult in championship.GetAllResults().SelectMany(x => x.DriverSessionResult))
            {
                driverResult.DriverName = driverDictionary[driverResult.DriverGuid].LastUsedName;
            }
        }

        private void AdvanceChampionship(ChampionshipDto championship)
        {
            EventDto currentEvent = championship.GetCurrentOrLastEvent();
            championship.CurrentSessionIndex = (championship.CurrentSessionIndex + 1) % currentEvent.Sessions.Count;
            if (championship.CurrentSessionIndex == 0)
            {
                currentEvent.EventStatus = EventStatus.Finished;
                championship.CurrentEventIndex++;
                currentEvent = championship.GetCurrentOrLastEvent();

                if (currentEvent.EventStatus == EventStatus.NotStarted)
                {
                    currentEvent.EventStatus = EventStatus.InProgress;
                }

                championship.NextTrack = currentEvent.TrackName;
                ReRollMysteryProperties(championship);
            }

            championship.ChampionshipState = championship.CurrentEventIndex >= championship.Events.Count ? ChampionshipState.Finished : ChampionshipState.Started;
        }

        public void ReRollMysteryProperties(ChampionshipDto championship)
        {
            EventDto currentEvent = championship.GetCurrentOrLastEvent();
            if (currentEvent.IsMysteryTrack)
            {
                List<string> usedTracks = championship.Events.Select(x => x.TrackName).Where(x => !string.IsNullOrWhiteSpace(x)).Distinct().ToList();
                Track track = _simulatorContentProvider.GetRandomTrack(championship.SimulatorName, usedTracks);

                if (track != null)
                {
                    currentEvent.TrackName = track.Name;
                    championship.NextTrack = currentEvent.TrackName;
                    currentEvent.Sessions
                        .Where(x => x.IsDistanceBased)
                        .ForEach(x => x.DistanceDescription =
                            $"{(int)Math.Round(x.DistanceInMeters / track.LapDistance)} Laps / {Distance.FromMeters(x.DistanceInMeters).GetByUnit(_displaySettingsViewModel.DistanceUnits)}{Distance.GetUnitsSymbol(_displaySettingsViewModel.DistanceUnits)}");
                }
            }

            if (championship.IsMysteryClass)
            {
                championship.ClassName = championship.AllowedClasses.Count == 0
                    ? _simulatorContentProvider.GetRandomClass(championship.SimulatorName)?.ClassName ?? string.Empty
                    : championship.AllowedClasses[_random.Next(championship.AllowedClasses.Count)];
            }
        }

        public void ResetChampionship(ChampionshipDto championship, bool reshuffleEvent)
        {
            championship.Events.SelectMany(x => x.Sessions).ForEach(x => x.SessionResult = null);
            championship.Events.ForEach(x => x.EventStatus = EventStatus.NotStarted);
            championship.CurrentEventIndex = 0;
            championship.CurrentSessionIndex = 0;
            championship.ChampionshipState = ChampionshipState.NotStarted;
            championship.Drivers.Clear();
            championship.ManufacturersChampionshipDtos.Clear();

            if (reshuffleEvent)
            {
                Random random = new Random();
                championship.Events = championship.Events.OrderBy(x => random.NextDouble()).ToList();
                championship.ResetEventNames();
            }

            EventDto firstEvent = championship.GetCurrentOrLastEvent();
            championship.NextTrack = firstEvent.TrackName;
        }

        private SessionResultDto CreateResultDto(ChampionshipDto championship, SimulatorDataSet dataSet, ILapInfo bestLap, double completionPercentage)
        {
            string bestLapDriverId = bestLap?.Driver.DriverId ?? string.Empty;
            ScoringDto scoring = championship.Scoring[championship.CurrentSessionIndex];
            Dictionary<string, int> positionMap = dataSet.GetDriversInfoByMultiClass().ToDictionary(x => x.DriverLongName, x => x.PositionInClass);
            SessionResultDto resultDto = new SessionResultDto()
            {
                CompletionPercentage = completionPercentage
            };

            Logger.Info($"Completed % is {completionPercentage}");

            if (completionPercentage < 1)
            {
                resultDto.AddRemark(GetSessionRemainingRemark(dataSet));
            }

            double pointsCoef = completionPercentage switch
            {
                > 0.75 => 1,
                > 0.25 => .5,
                _ => 0
            };

            foreach (DriverDto championshipDriver in championship.Drivers.Where(x => !x.IsInactive))
            {
                DriverInfo sessionDriver = dataSet.DriversInfo.FirstOrDefault(x => x.DriverLongName == championshipDriver.LastUsedName);
                if (sessionDriver == null)
                {
                    Logger.Error($"Driver {championshipDriver.LastUsedName} not found");
                    throw new InvalidOperationException($"Driver {championshipDriver.LastUsedName} not found");
                }

                int position = positionMap[sessionDriver.DriverLongName];
                DriverSessionResultDto driverResult = new()
                {
                    DriverGuid = championshipDriver.GlobalKey,
                    DriverName = championshipDriver.LastUsedName,
                    FinishPosition = position,
                    PointsGain = position <= scoring.Scoring.Count ? (int)(scoring.Scoring[position - 1] * pointsCoef) : 0,
                    BeforeEventPosition = championshipDriver.Position,
                    IsPlayer = championshipDriver.IsPlayer,
                    CarName = sessionDriver.CarDisplayName,
                    ClassId = sessionDriver.CarClassId,
                    ClassName = sessionDriver.CarClassName,
                    TeamName = sessionDriver.TeamName,
                };

                if (bestLapDriverId == sessionDriver.DriverSessionId && position <= scoring.MaxPositionForFastestLap)
                {
                    Logger.Info($"Fastest Lap for ${bestLapDriverId}, on position {position}");
                    driverResult.WasFastestLap = true;
                    driverResult.PointsGain += (int)(scoring.PointsForFastestLap * pointsCoef);
                }

                driverResult.TotalPoints = championshipDriver.TotalPoints + driverResult.PointsGain;

                resultDto.DriverSessionResult.Add(driverResult);
            }

            DriverSessionResultComparer comparer = new(championship, resultDto);
            List<DriverSessionResultDto> driversAfterRaceOrdered = resultDto.DriverSessionResult.OrderBy(x => x, comparer).ToList();
            for (int i = 0; i < driversAfterRaceOrdered.Count; i++)
            {
                driversAfterRaceOrdered[i].AfterEventPosition = i + 1;
            }

            AddTeamsResults(championship, resultDto);
            AddManufacturerResults(championship, resultDto);

            return resultDto;
        }

        private void AddManufacturerResults(ChampionshipDto championshipDto, SessionResultDto resultDto)
        {
            resultDto.ManufacturersSessionResult.Clear();
            if (!championshipDto.IsManufacturerScoringEnabled)
            {
                return;
            }

            ManufacturerMap<DriverSessionResultDto> manufacturerMap = _manufacturerProvider.Map(resultDto.DriverSessionResult, x => x.CarName);

            List<string> unknownCars = manufacturerMap.EntitiesWithUnknownParticipants.Select(x => x.CarName).Distinct().ToList();
            if (unknownCars.Count > 0)
            {
                string remark = "Unknown Car Manufacturers " + string.Join("\n", unknownCars);
                resultDto.AddRemark(remark);
            }

            int totalManufacturer = manufacturerMap.TotalManufacturers;
            if (totalManufacturer < 2)
            {
                return;
            }

            int minDriversPerManufacturer = manufacturerMap.Map.Select(x => x.Value.Count).Min();
            List<(Manufacturer Manufacturer, int Points)> manufacturersWithPoints = manufacturerMap.Map.Select(x => (x.Key,
                x.Value.OrderBy(driver => driver.FinishPosition).Take(minDriversPerManufacturer).Sum(driver => driver.PointsGain))).ToList();

            int currentPosition = 1;

            ManufacturersChampionshipDto manufacturersChampionshipDto = championshipDto.GetOrCreateManufacturerChampionship();

            // AddResults for manufacturers in event
            foreach ((Manufacturer manufacturer, int totalPointsForManufacturer) in manufacturersWithPoints.OrderByDescending(x => x.Points))
            {
                ManufacturerDto manufacturerDto = manufacturersChampionshipDto.GetOrCreateManufacturer(manufacturer.Name);
                ManufacturerSessionResultDto manufacturerSessionResultDto = new()
                {
                    IsPlayer = manufacturerDto.IsPlayer,
                    FinishPosition = currentPosition,
                    ManufacturerName = manufacturerDto.ManufacturerName,
                    PointsGain = totalPointsForManufacturer,
                    TotalPoints = manufacturerDto.TotalPoints + totalPointsForManufacturer,
                    BeforeEventPosition = manufacturerDto.Position,
                };

                resultDto.ManufacturersSessionResult.Add(manufacturerSessionResultDto);
                currentPosition++;
            }

            // AddResults for manufacturers not in Event
            foreach (ManufacturerDto manufacturerDto in manufacturersChampionshipDto.Manufacturers.Where(x =>
                         resultDto.ManufacturersSessionResult.All(y => y.ManufacturerName != x.ManufacturerName)))
            {
                ManufacturerSessionResultDto manufacturerSessionResultDto = new ManufacturerSessionResultDto()
                {
                    IsPlayer = manufacturerDto.IsPlayer,
                    FinishPosition = currentPosition,
                    ManufacturerName = manufacturerDto.ManufacturerName,
                    PointsGain = 0,
                    TotalPoints = manufacturerDto.TotalPoints,
                    BeforeEventPosition = manufacturerDto.Position,
                };

                resultDto.ManufacturersSessionResult.Add(manufacturerSessionResultDto);
                currentPosition++;
            }

            ManufacturerResultComparer comparer = new(championshipDto, resultDto);
            List<ManufacturerSessionResultDto> manufacturerOrderer = resultDto.ManufacturersSessionResult.OrderBy(x => x, comparer).ToList();
            for (int i = 0; i < manufacturerOrderer.Count; i++)
            {
                manufacturerOrderer[i].AfterEventPosition = i + 1;
            }
        }

        private void AddTeamsResults(ChampionshipDto championshipDto, SessionResultDto resultDto)
        {
            resultDto.TeamSessionResult.Clear();
            if (!championshipDto.IsTeamScoringEnabled)
            {
                return;
            }

            TeamsMap<DriverSessionResultDto> manufacturerMap = _teamsMapProvider.Map(resultDto.DriverSessionResult, x => x.TeamName);

            int totalTeams = manufacturerMap.TotalParticipants;
            if (totalTeams < 2)
            {
                return;
            }

            int minDriversPerManufacturer = manufacturerMap.Map.Select(x => x.Value.Count).Min();
            List<(string teaamName, int Points)> teamsWithPoints = manufacturerMap.Map.Select(x => (x.Key,
                x.Value.OrderBy(driver => driver.FinishPosition).Take(minDriversPerManufacturer).Sum(driver => driver.PointsGain))).ToList();

            int currentPosition = 1;

            TeamsChampionshipDto teamsChampionshipDto = championshipDto.GetOrCreateTeamChampionship();

            // AddResults for manufacturers in event
            foreach ((string manufacturer, int totalPointsForManufacturer) in teamsWithPoints.OrderByDescending(x => x.Points))
            {
                TeamDto teamDto = teamsChampionshipDto.GetOrCreateTeam(manufacturer);
                TeamSessionResultDto teamSessionResult = new()
                {
                    IsPlayer = teamDto.IsPlayer,
                    FinishPosition = currentPosition,
                    TeamName = teamDto.TeamName,
                    PointsGain = totalPointsForManufacturer,
                    TotalPoints = teamDto.TotalPoints + totalPointsForManufacturer,
                    BeforeEventPosition = teamDto.Position,
                };

                resultDto.TeamSessionResult.Add(teamSessionResult);
                currentPosition++;
            }

            // AddResults for manufacturers not in Event
            foreach (TeamDto teamDto in teamsChampionshipDto.TeamDtos.Where(x =>
                         resultDto.TeamSessionResult.All(y => y.TeamName != x.TeamName)))
            {
                TeamSessionResultDto teamSessionResult = new TeamSessionResultDto()
                {
                    IsPlayer = teamDto.IsPlayer,
                    FinishPosition = currentPosition,
                    TeamName = teamDto.TeamName,
                    PointsGain = 0,
                    TotalPoints = teamDto.TotalPoints,
                    BeforeEventPosition = teamDto.Position,
                };

                resultDto.TeamSessionResult.Add(teamSessionResult);
                currentPosition++;
            }

            TeamsResultComparer comparer = new(championshipDto, resultDto);
            List<TeamSessionResultDto> teamsOrdered = resultDto.TeamSessionResult.OrderBy(x => x, comparer).ToList();
            for (int i = 0; i < teamsOrdered.Count; i++)
            {
                teamsOrdered[i].AfterEventPosition = i + 1;
            }
        }

        private string GetSessionRemainingRemark(SimulatorDataSet dataSet)
        {
            return dataSet.SessionInfo.SessionLengthType switch
            {
                SessionLengthType.Laps => $"Stopped with {Math.Max(1, dataSet.SessionInfo.TotalNumberOfLaps - dataSet.LeaderInfo.CompletedLaps)} laps to go.",
                SessionLengthType.Time or SessionLengthType.TimeWithExtraLap =>
                    $"Stopped with {TimeSpan.FromSeconds(dataSet.SessionInfo.SessionTimeRemaining).FormatToHoursMinutesSeconds()} to go.",
                _ => string.Empty
            };
        }

        private void InitializeDrivers(ChampionshipDto championship, SimulatorDataSet dataSet)
        {
            int position = 0;
            IReadOnlyCollection<DriverInfo> eligibleDrivers = dataSet.GetDriversInfoByMultiClass();
            championship.ClassName = dataSet.PlayerInfo.CarClassName;
            championship.TotalDrivers = eligibleDrivers.Count;
            championship.Drivers = eligibleDrivers.Select(x => new DriverDto()
            {
                LastUsedName = x.DriverLongName,
                FirstUsedName = x.DriverLongName,
                IsPlayer = x.IsPlayer,
                Position = ++position,
                LastCarName = x.CarDisplayName,
                LastClassId = x.CarClassId,
                LastClassName = x.CarClassName,
                TeamName = x.TeamName,
            }).ToList();
        }
    }
}
