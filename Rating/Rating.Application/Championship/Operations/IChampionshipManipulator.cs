﻿namespace SecondMonitor.Rating.Application.Championship.Operations
{
    using Common.DataModel.Championship;
    using DataModel.Snapshot;

    using Timing.Common.SessionTiming.Drivers.Lap;

    public interface IChampionshipManipulator
    {
        void StartChampionship(ChampionshipDto championship, SimulatorDataSet dataSet);
        void StartNextEvent(ChampionshipDto championship, SimulatorDataSet dataSet);
        void AddResultsForCurrentSession(ChampionshipDto championship, SimulatorDataSet dataSet, ILapInfo bestLap, double completionPercentage);
        void UpdateDriversProperties(ChampionshipDto championship, SimulatorDataSet dataSet);
        void UpdateAiDriversNames(ChampionshipDto championship, SimulatorDataSet dataSet);

        void DnfPlayerAndCommitLastSessionResults(ChampionshipDto championship);
        void CommitLastSessionResults(ChampionshipDto championship);

        void ReRollMysteryProperties(ChampionshipDto championship);
        void ResetChampionship(ChampionshipDto championship, bool shuffleCalendar);
    }
}