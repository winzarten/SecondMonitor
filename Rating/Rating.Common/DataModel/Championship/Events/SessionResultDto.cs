﻿namespace SecondMonitor.Rating.Common.DataModel.Championship.Events
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;

    [Serializable]
    public class SessionResultDto
    {
        public SessionResultDto()
        {
            DriverSessionResult = new List<DriverSessionResultDto>();
            ManufacturersSessionResult = new List<ManufacturerSessionResultDto>();
            TeamSessionResult = new List<TeamSessionResultDto>();
            ResultCreationTime = DateTime.UtcNow;
            Remarks = string.Empty;
            CompletionPercentage = 1.0;
        }

        public List<DriverSessionResultDto> DriverSessionResult { get; set; }

        public List<ManufacturerSessionResultDto> ManufacturersSessionResult { get; set; }

        public List<TeamSessionResultDto> TeamSessionResult { get; set; }

        [XmlIgnore]
        public bool HasManufacturersResults => ManufacturersSessionResult.Count > 0;

        [XmlAttribute]
        public DateTime ResultCreationTime { get; set; }

        [XmlAttribute]
        public string Remarks { get; set; }

        /// <summary>
        /// <remarks>0 - 1 Range</remarks>
        /// </summary>
        [XmlAttribute]
        public double CompletionPercentage { get; set; }

        public void AddRemark(string remark)
        {
            if (!string.IsNullOrWhiteSpace(Remarks))
            {
                Remarks += "\n";
            }

            Remarks += remark;
        }
    }
}