﻿namespace SecondMonitor.Rating.Common.DataModel.Championship
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Xml.Serialization;
    using Events;

    [Serializable]
    public partial class ChampionshipDto
    {
        public ChampionshipDto()
        {
            AllowedClasses = new List<string>();
            ChampionshipGlobalId = Guid.NewGuid();
            Events = new List<EventDto>();
            Scoring = new List<ScoringDto>();
            Drivers = new List<DriverDto>();
            CreationDateTime = DateTime.UtcNow;
            ManufacturersChampionshipDtos = new List<ManufacturersChampionshipDto>();
            TeamsChampionshipDtos = new List<TeamsChampionshipDto>();
        }

        public DateTime CreationDateTime { get; set; }

        public List<EventDto> Events { get; set; }

        [XmlAttribute]
        public string SimulatorName { get; set; }

        [XmlAttribute]
        public Guid ChampionshipGlobalId { get; set; }

        [XmlAttribute]
        public string ChampionshipName { get; set; }

        [XmlAttribute]
        public string NextTrack { get; set; }

        [XmlAttribute]
        public ChampionshipState ChampionshipState { get; set; }

        [XmlAttribute]
        public int TotalEvents { get; set; }

        [XmlAttribute]
        public int CurrentEventIndex { get; set; }

        [XmlAttribute]
        public int CurrentSessionIndex { get; set; }

        [XmlIgnore]
        public int Position => Drivers?.FirstOrDefault(x => x.IsPlayer)?.Position ?? 0;

        [XmlAttribute]
        public int TotalDrivers { get; set; }

        [XmlAttribute]
        public string ClassName { get; set; }

        [XmlAttribute]
        public bool AiNamesCanChange { get; set; }

        [XmlAttribute]
        public bool IsClassChangeEnabled { get;  set; }

        [XmlAttribute]
        public bool IsMysteryClass { get; set; }

        [XmlAttribute]
        public bool IsManufacturerScoringEnabled { get; set; }

        [XmlAttribute]
        public bool IsTeamScoringEnabled { get; set; }

        public List<ScoringDto> Scoring { get; set; }

        public List<DriverDto> Drivers { get; set; }

        public List<ManufacturerDto> Manufacturers { get; set; }

        public List<ManufacturersChampionshipDto> ManufacturersChampionshipDtos { get; set; }

        public List<TeamsChampionshipDto> TeamsChampionshipDtos { get; set; }

        public List<string> AllowedClasses { get; set; }

        [XmlIgnore]
        public bool HasTeamsChampionships => IsTeamScoringEnabled && GetOrCreateTeamChampionship().TeamDtos.Count > 1;

        [XmlIgnore]
        public int CompletedRaces => (CurrentEventIndex * Events[0].Sessions.Count) + CurrentSessionIndex + 1;

        [XmlIgnore]
        public DateTime LastRunDateTime => GetAllResults().OrderBy(x => x.ResultCreationTime).LastOrDefault()?.ResultCreationTime ?? CreationDateTime;

        public Dictionary<Guid, DriverDto> GetGuidToDriverDictionary() => Drivers.ToDictionary(x => x.GlobalKey, x => x);

        public EventDto GetCurrentOrLastEvent()
        {
            return CurrentEventIndex < Events.Count ? Events[CurrentEventIndex] : Events.Last();
        }

        public EventDto GetCurrentEvent()
        {
            return CurrentEventIndex < Events.Count ? Events[CurrentEventIndex] : null;
        }

        public IEnumerable<SessionDto> GetAllSessions()
        {
            return Events.SelectMany(x => x.Sessions);
        }

        public IEnumerable<SessionResultDto> GetAllResults()
        {
            return Events.SelectMany(x => x.Sessions).Select(x => x.SessionResult).Where(x => x != null);
        }

        public (EventDto EventDto, SessionDto SessionDto) GetLastSessionWithResults()
        {
            var lastEvent = Events.LastOrDefault(x => x.Sessions.Any(y => y.SessionResult != null));
            return lastEvent == null ? (null, null) : (lastEvent, lastEvent.Sessions.Last(x => x.SessionResult != null));
        }

        public void ResetEventNames()
        {
            for (int i = 0; i < Events.Count; i++)
            {
                EventDto currentEvent = Events[i];
                if (MyRegex().IsMatch(currentEvent.EventName))
                {
                    currentEvent.EventName = $"Round {i + 1}";
                }
            }
        }

        public ManufacturersChampionshipDto GetOrCreateManufacturerChampionship(string className, string classId)
        {
            // AMS2 LD Variants should be tracked in the same manufacturer championship
            if (classId.EndsWith("_LD"))
            {
                classId = classId.Replace("_LD", string.Empty);
                className = className.Replace("_LD", string.Empty);
            }

            ManufacturersChampionshipDto manufacturerDto = ManufacturersChampionshipDtos.SingleOrDefault(x => x.ClassId == classId);
            if (manufacturerDto != null)
            {
                return manufacturerDto;
            }

            manufacturerDto = new ManufacturersChampionshipDto()
            {
                ClassId = classId,
                ClassName = className,
            };

            ManufacturersChampionshipDtos.Add(manufacturerDto);

            return manufacturerDto;
        }

        public TeamsChampionshipDto GetOrCreateTeamChampionship(string className, string classId)
        {
            // AMS2 LD Variants should be tracked in the same manufacturer championship
            if (classId.EndsWith("_LD"))
            {
                classId = classId.Replace("_LD", string.Empty);
                className = className.Replace("_LD", string.Empty);
            }

            TeamsChampionshipDto teamsChampionshipDto = TeamsChampionshipDtos.SingleOrDefault(x => x.ClassId == classId);
            if (teamsChampionshipDto != null)
            {
                return teamsChampionshipDto;
            }

            teamsChampionshipDto = new TeamsChampionshipDto()
            {
                ClassId = classId,
                ClassName = className,
            };

            TeamsChampionshipDtos.Add(teamsChampionshipDto);

            return teamsChampionshipDto;
        }

        public ManufacturersChampionshipDto GetOrCreateManufacturerChampionship()
        {
            DriverDto player = Drivers.Single(x => x.IsPlayer);
            return GetOrCreateManufacturerChampionship(player.LastClassName, player.LastClassId);
        }

        public TeamsChampionshipDto GetOrCreateTeamChampionship()
        {
            DriverDto player = Drivers.Single(x => x.IsPlayer);
            return GetOrCreateTeamChampionship(player.LastClassName, player.LastClassId);
        }

        [GeneratedRegex(@"Round \d+")]
        private static partial Regex MyRegex();
    }
}