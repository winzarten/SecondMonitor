﻿namespace SecondMonitor.Rating.Common.Factories
{
    using System.Collections.Generic;
    using System.Linq;

    using SecondMonitor.DataModel.BasicProperties;
    using SecondMonitor.DataModel.Extensions;
    using SecondMonitor.DataModel.Summary;

    public class PitCompensateAveragePositionAdapter(IDriverAveragePositionAdapter backupAdapter) : IDriverAveragePositionAdapter
    {
        public IReadOnlyDictionary<string, double> GetAveragePositions(SessionSummary sessionSummary)
        {
            Driver player = sessionSummary.Drivers.FirstOrDefault(x => x.IsPlayer);

            if (sessionSummary.SessionType != SessionType.Race || player == null || sessionSummary.PitStops.Count == 0)
            {
                return backupAdapter.GetAveragePositions(sessionSummary);
            }

            List<Driver> eligibleDrivers = sessionSummary.Drivers.Where(x => x.ClassId == player.ClassId).ToList();
            int lapsToConsider = eligibleDrivers.Max(x => x.Laps.Count);
            List<DriverAverageByLapTimes> driverAverageByLapTimesList = eligibleDrivers.Select(x => new DriverAverageByLapTimes(x, lapsToConsider)).ToList();

            for (int i = 0; i < lapsToConsider; i++)
            {
                List<(DriverAverageByLapTimes item, int index)> driverAverageByLapTimesListOrdered = driverAverageByLapTimesList.OrderBy(x => x.TotalTimes[i])
                    .Select((item, index) => (item, index))
                    .ToList();
                driverAverageByLapTimesListOrdered.ForEach(x => x.item.Positions.Add(x.index + 1));
            }

            Dictionary<string, double> driverToAvgPosition =
                driverAverageByLapTimesList.ToDictionary(x => x.Driver.DriverId, x => x.Positions.Average(position => (double)position));

            sessionSummary.Drivers.Where(x => x.ClassId != player.ClassId).ForEach(x => driverToAvgPosition.Add(x.DriverId, double.MaxValue));

            return driverToAvgPosition;
        }
    }
}