﻿namespace SecondMonitor.ViewModels.Track
{
    public enum SectorState
    {
        Normal,
        Yellow,
        Purple,
        ClassPurple,
        PersonalBest
    }
}