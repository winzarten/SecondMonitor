﻿namespace SecondMonitor.ViewModels.Settings.ViewModel.Migrations
{
    using Model;

    using SecondMonitor.ViewModels.Repository;

    public interface ISettingMigration : IDtoMigration<DisplaySettings>
    {
    }
}