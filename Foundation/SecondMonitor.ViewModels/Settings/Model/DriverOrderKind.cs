﻿namespace SecondMonitor.ViewModels.Settings.Model
{
    public enum DriverOrderKind
    {
        Absolute,
        Relative,
        AbsoluteByClass,
        HybridByClass,
        Hybrid,
    }
}