﻿namespace SecondMonitor.ViewModels.Settings.Model.Layout
{
    public enum SizeKind
    {
        Automatic,
        Remaining,
        Manual
    }
}