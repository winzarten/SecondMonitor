﻿namespace SecondMonitor.ViewModels.Settings.Model
{
    using Layouts;

    public class LayoutSettings
    {
        public LayoutDescription Description { get; set; }
    }
}