﻿namespace SecondMonitor.ViewModels.Settings.Model
{
    public class WindowLocationSetting
    {
        public int WindowState { get; set; }
        public double Left { get; set; }
        public double Top { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
    }
}