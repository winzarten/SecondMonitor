﻿namespace SecondMonitor.ViewModels.Settings.Model
{
    public enum HorizontalAlignment
    {
        Left,
        Right,
        Center
    }
}