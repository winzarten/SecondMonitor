﻿namespace SecondMonitor.ViewModels.Settings.Model
{
    public enum LapReferenceKind
    {
        None, SessionOverallBest, SessionClassBest, PlayerBest, LastLap
    }
}
