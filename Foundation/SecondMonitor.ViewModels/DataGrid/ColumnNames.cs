﻿namespace SecondMonitor.ViewModels.DataGrid
{
    public static class ColumnNames
    {
        public static string CarRaceNumber => "Car Race Number";
        public static string TeamName => "Team Name";
    }
}