﻿namespace SecondMonitor.ViewModels.CarStatus.FuelStatus
{
    public enum HybridEnduranceState
    {
        Ok,
        Warning,
        Error,
    }
}
