﻿namespace SecondMonitor.ViewModels.CarStatus.FuelStatus
{
    using System.Collections.Generic;

    using SecondMonitor.DataModel;
    using SecondMonitor.DataModel.Snapshot;

    public class ConsumptionMonitorFactory : IConsumptionMonitorFactory
    {
        public IReadOnlyList<IConsumptionMonitor> Create(SimulatorDataSet dataSet)
        {
            List<IConsumptionMonitor> consumptionMonitors = new List<IConsumptionMonitor>() { new FuelConsumptionMonitor() };
            if (dataSet.Source == SimulatorsNameMap.LmUSimName && dataSet.PlayerInfo.CarClassName is SimulatorsNameMap.LmUHyperCarClassId or SimulatorsNameMap.LmUGT3CarClassId)
            {
                consumptionMonitors.Add(new VirtualEnergyConsumptionMonitor());
            }

            return consumptionMonitors;
        }
    }
}
