﻿namespace SecondMonitor.ViewModels.CarStatus
{
    public enum IndicationStatus
    {
        NoIndication,
        CurrentlyIndicating,
        WasIndicating,
    }
}