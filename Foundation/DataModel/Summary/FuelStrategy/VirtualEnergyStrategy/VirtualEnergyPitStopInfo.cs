﻿namespace SecondMonitor.DataModel.Summary.FuelStrategy.VirtualEnergyStrategy
{
    public enum VirtualEnergyPitStopInfo
    {
        TotalFuelEndOfLap, TotalFuelPitLap, FuelToAdd 
    }
}
