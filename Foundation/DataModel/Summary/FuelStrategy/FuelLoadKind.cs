﻿namespace SecondMonitor.DataModel.Summary.FuelStrategy
{
    public enum FuelLoadKind
    {
        Auto, FullTank, Custom
    }
}
