﻿namespace SecondMonitor.DataModel.Summary.FuelStrategy
{
    public enum StintLengthKind
    {
        Equal, LastShort
    }
}
