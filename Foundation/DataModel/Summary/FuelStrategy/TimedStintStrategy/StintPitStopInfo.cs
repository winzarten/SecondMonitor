﻿namespace SecondMonitor.DataModel.Summary.FuelStrategy.TimedStintStrategy
{
    public enum StintPitStopInfo
    {
        FuelAtEndOfLap, FuelAtPitLap, Both
    }
}
