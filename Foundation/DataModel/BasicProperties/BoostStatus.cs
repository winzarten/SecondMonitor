﻿namespace SecondMonitor.DataModel.BasicProperties
{
    public enum BoostStatus
    {
        UnAvailable,
        Available,
        InUse,
        Cooldown,
        Balanced,
        Attack,
        Build,
        Qualification,
        Off,
    }
}