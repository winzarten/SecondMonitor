﻿namespace SecondMonitor.DataModel.BasicProperties.Units
{
    public class UnitsCollection
    {
        public DistanceUnits DistanceUnits { get; set; }
        public DistanceUnits DistanceUnitsVerySmall { get; set; }
        public VelocityUnits VelocityUnits { get; set; }
        public VelocityUnits VelocityUnitsSmall { get; set; }
        public TemperatureUnits TemperatureUnits { get; set; }
        public PressureUnits PressureUnits { get; set; }
        public AngleUnits AngleUnits { get; set; }
        public ForceUnits ForceUnits { get; set; }
    }
}