﻿namespace SecondMonitor.DataModel.BasicProperties
{
    public enum SessionBestInformationKind
    {
        BestOverall, BestOfClass, BestOverallAndClass, AllClasses
    }
}
