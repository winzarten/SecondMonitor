﻿namespace SecondMonitor.DataModel.Snapshot.Systems
{
    public enum HybridChargeChange
    {
        Unavailable,
        None,
        Discharge,
        Recharge,
    }
}
