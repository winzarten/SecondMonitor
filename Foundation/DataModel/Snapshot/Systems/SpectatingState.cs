﻿namespace SecondMonitor.DataModel.Snapshot.Systems
{
    public enum SpectatingState
    {
        Live,
        Spectate,
        Replay
    }
}