﻿namespace SecondMonitor.DataModel.Snapshot.Systems
{
    public enum HybridSystemMode
    {
        UnAvailable,
        Off,
        Balanced,
        Attack,
        Build,
        Qualification,
    }
}
