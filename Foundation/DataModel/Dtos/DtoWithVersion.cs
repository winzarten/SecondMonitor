﻿namespace SecondMonitor.DataModel.Dtos
{
    public interface IDtoWithVersion
    {
        int Version { get; set; }
    }
}
