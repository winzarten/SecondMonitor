﻿namespace SecondMonitor.WindowsControls.WPF.DriverPosition
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for MapSidePanelControl.xaml
    /// </summary>
    public partial class MapSidePanelControl : UserControl
    {
        public MapSidePanelControl()
        {
            InitializeComponent();
        }
    }
}
