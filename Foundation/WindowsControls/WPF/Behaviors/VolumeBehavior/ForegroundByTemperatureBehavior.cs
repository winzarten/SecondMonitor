﻿namespace SecondMonitor.WindowsControls.WPF.Behaviors.VolumeBehavior
{
    using DataModel.BasicProperties;

    public class ForegroundByTemperatureBehavior : ForegroundByOptimalVolumeBehavior<Temperature>
    {
    }
}