﻿namespace SecondMonitor.WindowsControls.WPF.Behaviors.VolumeBehavior
{
    using DataModel.BasicProperties;

    public class BackgroundByTemperatureBehavior : BackgroundByOptimalVolumeBehavior<Temperature>
    {
        public BackgroundByTemperatureBehavior()
        {
        }
    }
}