﻿namespace SecondMonitor.WindowsControls.WPF.PluginsConfig
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for BroadcastLimitSettingsControl.xaml
    /// </summary>
    public partial class BroadcastLimitSettingsControl : UserControl
    {
        public BroadcastLimitSettingsControl()
        {
            InitializeComponent();
        }
    }
}
