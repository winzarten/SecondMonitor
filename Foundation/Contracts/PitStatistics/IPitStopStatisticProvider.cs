﻿namespace SecondMonitor.Contracts.PitStatistics
{
    public interface IPitStopStatisticProvider
    {
        PitStatistics GetCurrentStatisticsPlayerClass();
    }
}