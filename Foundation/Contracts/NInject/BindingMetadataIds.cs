﻿namespace SecondMonitor.Contracts.NInject
{
    public static class BindingMetadataIds
    {
        public const string SimulatorNameBinding = "SimulatorName";
        public const string SeriesChartsNameBinding = "SeriesChartName";
        public const string AggregatedChartProviderName = "AggregatedChartProviderName";
        public const string DriversOrdering = "DriversOrdering";
    }
}