﻿namespace SecondMonitor.Contracts.NInject
{
    public static class NamedScopes
    {
        public const string TimingApplicationScope = "TimingApplicationScope";

        public const string SessionScope = "SessionScope";
    }
}