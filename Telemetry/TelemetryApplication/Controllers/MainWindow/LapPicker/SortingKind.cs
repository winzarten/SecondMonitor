﻿namespace SecondMonitor.Telemetry.TelemetryApplication.Controllers.MainWindow.LapPicker
{
    public enum SortingKind
    {
        PlayerFirstByTime,
        PlayerFirstByLap,
        ByTime,
        ByLap,
        GroupByDriverByLap,
        GroupByDriverTime,
    }
}