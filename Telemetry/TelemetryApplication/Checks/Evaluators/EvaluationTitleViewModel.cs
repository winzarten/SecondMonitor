﻿namespace SecondMonitor.Telemetry.TelemetryApplication.Checks.Evaluators
{
    using SecondMonitor.ViewModels.Text;

    public class EvaluationTitleViewModel : TitleViewModel
    {
        public EvaluationResultKind EvaluationResultKind { get; set; }
    }
}