﻿namespace SecondMonitor.Timing.Common.SessionTiming.Drivers
{
    public enum DriverPositionChange
    {
        None, Gained, Lost,
    }
}
