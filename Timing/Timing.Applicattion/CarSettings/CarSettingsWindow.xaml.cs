﻿namespace SecondMonitor.Timing.Application.CarSettings
{
    using System.Windows;

    /// <summary>
    /// Interaction logic for CarSettingsWindow.xaml
    /// </summary>
    public partial class CarSettingsWindow : Window
    {
        public CarSettingsWindow()
        {
            InitializeComponent();
        }
    }
}
