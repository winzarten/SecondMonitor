﻿namespace SecondMonitor.Timing.Application.TrackRecords.Controller.Migrations
{
    using SecondMonitor.DataModel.TrackRecords;
    using SecondMonitor.ViewModels.Repository;

    public interface ISimulatorsRecordsMigration : IDtoMigration<SimulatorsRecords>
    {
    }
}
