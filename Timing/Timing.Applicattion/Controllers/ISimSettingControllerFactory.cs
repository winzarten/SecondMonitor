﻿namespace SecondMonitor.Timing.Application.Controllers
{
    public interface ISimSettingControllerFactory
    {
        SimSettingController CreateSimSettingController();
    }
}
