﻿namespace SecondMonitor.Plugins.Remote
{
    public interface IBroadcastServerConfiguration
    {
        int Port { get; }
    }
}