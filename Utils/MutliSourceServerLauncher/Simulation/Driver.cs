﻿namespace SecondMonitor.Remote.Debug.Simulation
{
    public class Driver
    {
        public Driver(string name)
        {
            Name = name;
        }

        public string Name { get; }
    }
}